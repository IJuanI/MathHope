#include "Expresion.h"

Expresion &Unknown::operator*(size_t value) const {
	return Expresion() += MathTerm() << Component(*this) << Component(value);
}

Expresion &Unknown::operator^(size_t value) const {
	return Expresion() += MathTerm() += Component(*this, value);
}

std::vector<Unknown> _U_storage;

Unknown getUnknown(char key) {

	for (Unknown unknown : _U_storage)
		if (unknown == key)
			return unknown;

	Unknown unknown(key);
	_U_storage.emplace_back(unknown);
	return unknown;
}