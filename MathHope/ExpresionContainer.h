#ifndef _ExpresionContainer
#define _ExpresionContainer
#include "BracketContainer.h"
#include "SimpleInterpreter.h"

class Interpreter;

namespace m_Interpreter {
	class ExpresionContainer {
		size_t e_level = 0;

		BracketContainer* e_broRoot = nullptr;
		Expresion e_content;
		vector<ExpresionContainer*> e_childs;
		ExpresionContainer* e_parent = nullptr;
		vector<size_t> e_dir;

		friend class ::Interpreter;

		void scan(vector<size_t> &path);
		void processLevel(size_t *current);
		void getChildsAtLevel(const size_t &level, vector<vector<size_t>> &result);
		BracketContainer* getBrother(const vector<size_t> &dir);

		void m_analize(string raw);
		void finalize();
	public:

		ExpresionContainer() { }
		ExpresionContainer(BracketContainer *brother);

		bool operator==(const ExpresionContainer &other) const;
		bool operator!=(const ExpresionContainer &other) const;

		ExpresionContainer* getChild(size_t index);
		ExpresionContainer* getChild(const vector<size_t> &path);
		ExpresionContainer &analize(string raw);

		~ExpresionContainer();
	};
}
#endif