#include "Expresion.h"
#include "Simplifier.h"

MathTerm::MathTerm() {
	clear();
}

MathTerm::MathTerm(const bool &sign) {
	clear();
	this->t_sign = sign;
}

MathTerm::MathTerm(Value &value) {
	operator+=(Component(value));
}

MathTerm::MathTerm(const Component &component) {
	operator+=(component);
}

MathTerm::MathTerm(const Component &value, const bool &sign) {
	operator+=(value);
	this->t_sign = sign;
}

MathTerm &MathTerm::operator+=(const Component &comp) {
	if (t_count >= t_components.size())
		t_components.resize(t_count + 1);

	if (m_isKnown && (!comp.getValue().isKnown() || !comp.getExponent().isKnown()))
		m_isKnown = false;
	m_isCompact = false;

	t_components[t_count] = comp;

	t_count++;

	m_isEmpty = false;

	return *this;
}

MathTerm &MathTerm::operator-=(const Component &comp) {
	m_isKnown = true;
	bool remove = false;
	for (size_t i = 0; i < t_count; i++) {
		if (remove)
			t_components[i - 1] = t_components[i];

		Component &component = t_components[i];
		if (component == comp)
			remove |= true;
	}

	if (remove) {
		t_count--;
		t_components.pop_back();

		if (t_count == 0) {
			m_isEmpty = true;
			t_components.resize(1);
			t_components[0] = Component(0);
		}

		m_isCompact = m_isEmpty;
	}

	return *this;
}

MathTerm &MathTerm::operator<<(const Component &comp) {
	return operator+=(comp);
}

MathTerm &MathTerm::operator>>(const Component &comp) {
	return operator-=(comp);
}

Component &MathTerm::operator[](size_t index) {
	return t_components[index];
}

MathTerm MathTerm::operator*(size_t value) const {
	MathTerm base;
	Component aux(1);

	for (Component comp : t_components)
		if (comp.isCompactable())
			aux *= comp.getCompact();
		else
			base += comp;

	base += aux;

	return base;
}

MathTerm MathTerm::operator*(const MathTerm &term) const {
	if (term.m_isEmpty || (term.t_count == 1 && term.getComponents()[0].getValue() == Value::ZERO()))
		return MathTerm();

	MathTerm result(t_sign == term.t_sign);

	std::vector<Component> comps;
	Component free;
	comps.insert(comps.end(), t_components.begin(), t_components.end());
	comps.insert(comps.end(), term.t_components.begin(), term.t_components.end());
	
	for (Component comp : comps)
		if (comp.isCompactable())
			free *= comp.getCompact();
		else {
			ptrdiff_t index = result.index(comp.getValue());
			if (index == -1)
				result += comp;
			else
				result[index].sumToExponent(comp.getExponent());
		}

	result += free;

	return result;
}

MathTerm &MathTerm::operator*=(size_t value) {
	for (size_t i = 0; i < t_count; i++)
		if (t_components[i].isCompactable()) {
			t_components[i] = Component(t_components[i].getCompact()) *= value;
			return *this;
		}
	operator+=(Component(value));
	return *this;
}

MathTerm &MathTerm::operator*=(const Unknown &unknown) {
	if (!m_isEmpty) {
		for (Component comp : t_components)
			if (comp.getValue() == unknown) {
				comp.sumToExponent(1);
				return *this;
			}
		operator+=(Component(unknown));
	}
	return *this;
}

MathTerm &MathTerm::operator*=(const Value &value) {
	if (!m_isEmpty) {
		if (value.isComplex()) {
			Expresion &complex = *value.complexValue;
			if (complex.m_count != 1) {
				Expresion result = Expresions::multiply(*this, complex.m_terms);
				clear();
				operator+=(Component(result));
			} else if (complex.m_count == 0)
				clear();
			else
				for (Component component : complex[0].t_components)
					if (component.isCompactable())
						multiplyKnown(component.getCompact());
					else {
						ptrdiff_t i = index(component.getValue());
						if (i == -1)
							operator+=(component);
						else
							t_components.at(i).sumToExponent(component.getExponent());
					}
		} else if (value.isKnown())
			return multiplyKnown(value);
		else {
			for (Component comp : t_components)
				if (comp.getValue() == value) {
					comp.sumToExponent(1);
					return *this;
				}
			operator+=(Component(value));
		}
	}

	return *this;
}

MathTerm &MathTerm::operator*=(const MathTerm &term) {
	if (!m_isEmpty) {
		if (term.m_isEmpty)
			clear();
		else
			for (Component comp : term.t_components) {
				if (comp.isCompactable())
					operator*=(comp.getCompact());
				else {
					ptrdiff_t i = index(comp.getValue());
					if (i == -1)
						operator+=(comp);
					else
						t_components.at(i).sumToExponent(comp.getExponent());
				}
			}
	}

	return *this;
}

MathTerm &MathTerm::operator*=(Expresion &expresion) {
	if (!m_isEmpty) {
		if (expresion.m_count != 1) {
			Expresion result = Expresions::multiply(*this, expresion.m_terms);
			clear();
			operator+=(Component(result));
		} else if (expresion.m_count == 0)
			clear();
		else
			for (Component component : expresion[0].t_components)
				if (component.isCompactable())
					multiplyKnown(component.getCompact());
				else {
					ptrdiff_t i = index(component.getValue());
					if (i == -1)
						operator+=(component);
					else
						t_components.at(i).sumToExponent(component.getExponent());
				}
	}
	return *this;
}

std::vector<Component> MathTerm::getComponents() const {
	return std::vector<Component>(t_components);
}
size_t MathTerm::size() {
	return t_components.size();
}

bool MathTerm::addToKnown(size_t value) { //Suspicious
	if (m_isKnown)
		if (t_components.size() > 0)
			t_components[0] += value;
		else
			return false;
	return true;
}

MathTerm &MathTerm::add(const Value &value) {
	if (!m_isEmpty) {
		compactate();
		if (m_isCompact || t_components.size() == 1)
			t_components[0] += value;
		else {
			MathTerm clone = MathTerm(*this);
			clear();
			operator+=(Component(Expresion(clone) += MathTerm(Component(value))));
		}
	}

	return *this;
}

MathTerm &MathTerm::multiplyKnown(const Value &known) {
	if (!known.isKnown())
		return *this;

	for (size_t i = 0; i < t_count; i++)
		if (t_components[i].isCompactable()) {
			t_components[i] = Component(t_components[i].getCompact()) *= known;
			return *this;
		}
	operator+=(Component(known));
	return *this;
}

bool MathTerm::isMember(Component &component) {
	return Utils::find(t_components, component) != t_components.end();
}

ptrdiff_t MathTerm::index(const Component &component) const {
	auto iter = Utils::findConst(t_components, component);
	if (iter == t_components.end())
		return -1;
	return std::distance(t_components.begin(), iter);
}

ptrdiff_t MathTerm::index(const Value &value) const {
	auto iter = Utils::findConst(t_components, (Func<Component>) ([&value](Component comp)
		{ return comp.getValue() == value; }));
	if (iter == t_components.end())
		return -1;
	return std::distance(t_components.begin(), iter);
}

MathTerm MathTerm::getCompact() const {
	MathTerm result(t_sign);
	result.m_isKnown = m_isKnown;

	if (m_isKnown && !m_isCompact) {
		if (!m_isEmpty) {
size_t val = 1;
for (Component comp : t_components)
if (comp.isCompactable())
val *= *comp.getCompact().healtyValue;
else
return false;
result << Component(val);
		}
	}
 else if (!m_isEmpty) {
	 bool changes = false;

	 for (Component comp : t_components) {
		 ptrdiff_t index = result.index(comp);
		 if (index != -1) {
			 result[index] *= comp;
			 changes |= true;
			 continue;
		 }

		 result << Component(comp);
	 }
 }

 result.m_isCompact = true;
 return result;
}

bool MathTerm::compactate() {
	if (m_isKnown && !m_isCompact) {
		if (!m_isEmpty) {
			size_t val = 1;
			for (Component comp : t_components)
				if (comp.isCompactable())
					val *= *comp.getCompact().healtyValue;
				else
					return false;
			clear();
			operator+=(Component(val));
		}
		m_isCompact = true;
	}
	else if (!m_isEmpty) {
		std::vector<Component> newComps;
		bool changes = false;

		for (Component comp : t_components) {
			for (Component stored : newComps)
				if (stored.getValue() == comp.getValue()) {
					stored *= comp;
					changes |= true;
					continue;
				}
			newComps.emplace_back(comp);
		}

		if (changes)
			t_components = newComps;
	}

	return m_isCompact;
}

void MathTerm::clear() {
	t_components.clear();
	t_components.resize(1);
	t_components[0] = Component(0);
}

bool MathTerm::unsortedEqual(const MathTerm &term) const {
	if (term.t_sign != t_sign)
		return false;

	std::vector<Component> pendient = getComponents();
	for (Component comp : term.getComponents()) {
		auto iter = find(pendient, comp);
		if (iter == pendient.end())
			return false;
		else
			pendient.erase(iter);
	}
	return pendient.empty();
}

bool MathTerm::checkEasyEqual(const MathTerm &term) const {
	if (term.t_sign != t_sign)
		return false;

	if (t_count == term.t_count) {
		for (size_t i = 0; i < t_count; i++)
			if (!(t_components.at(i) == term.t_components[i]))
				return false;
		return true;
	}

	return false;
}

std::vector<char> MathTerm::toString(bool child) const {
	std::vector<char> result;
	if (!child || !t_sign)
		result.push_back(t_sign ? '+' : '-');
	bool first = true;
	Component last;

	for (Component comp : t_components)
		if (t_count < 2 || !comp.isCompactable() || comp.getValue() != Value::ONE()) {
			if (first)
				first = false;
			else if (!comp.getValue().isComplex() && (last.hasExponent() || (last.getValue().isKnown() && comp.getValue().isKnown())))
				result.push_back('*');

			std::vector<char> parsed = comp.toString();

			bool _brackets = brackets(parsed);
			if (_brackets)
				result.push_back('(');

			result.insert(result.end(), parsed.begin(), parsed.end());

			if (_brackets)
				result.push_back(')');

			last = comp;
		}

	return result;
}

std::string MathTerm::toHTMLString(bool child, std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const {
	std::string result;
	if (!child || !t_sign)
		result.push_back(t_sign ? '+' : '-');
	bool first = true;
	Component last;

	for (Component comp : t_components)
		if (t_count < 2 || !comp.isCompactable() || comp.getValue() != Value::ONE()) {
			if (first)
				first = false;
			else if (!comp.getValue().isComplex() && (last.hasExponent() || (last.getValue().isKnown() && comp.getValue().isKnown())))
				result.push_back('*');

			bool _brackets = comp.getValue().isComplex() && (comp.getValue().complexValue->m_count > 1 || !comp.getValue().complexValue->operator[](0).t_sign);
			if (_brackets)
				result.push_back('(');

			std::string parsed = comp.toHTMLString(result.size() + overall, powIndexes);

			result.insert(result.end(), parsed.begin(), parsed.end());

			if (_brackets)
				result.push_back(')');

			last = comp;
		}

	return result;
}

