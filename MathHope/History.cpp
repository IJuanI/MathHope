#include "History.h"
#include "MathInput.h"
#include <vector>
#include <iostream>

using namespace std;

History::History(wxWindow *parent, MathInput *input, wxWindowID id, long style)
	: wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize, style), input(input) { }

void History::OnRightClick(wxListEvent &event) {
	input->SetFromHistory(event.GetItem().GetText());
}