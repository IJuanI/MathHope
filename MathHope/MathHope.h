#ifndef _MathHope
#define _MathHope

//Std Includes
#include <iostream>

//MathHope lib includes
#include "Expresion.h"
#include "Interpreter.h"
#include "Amplificator.h"
#include "Asociator.h"
#include "Simplifier.h"

//wxWidgets includes
#include "wx/wxprec.h"
#ifndef WX_PRECOMP
	#include "wx/wx.h"
#endif
#include <wx/gbsizer.h>
#include <wx/collpane.h>
#include <wx/fontpicker.h>
#include <wx/clrpicker.h>

//wxWidgets Custom includes
#include "MathList.h"
#include "MathInput.h"

//Other Includes
#include "Properties.h"

using namespace std;

namespace m_MathHope {
	bool &testMode();
}

wxString calculate(const std::string &primary, CalcMode mode);
bool compare(const std::string &primary, const std::string &secondary);
std::string readHTML(const std::string input);

class MathFrame;

struct MathHope : public wxApp {
	Properties props = Properties("MathHope.conf");
	virtual bool OnInit();
	virtual int OnExit();
};

class MathFrame : public wxFrame {
	//Properties
	std::size_t inputHeight = 40;
	wxFont globalFont = *wxSWISS_FONT;
	wxFont inputFont = *wxSWISS_FONT;
	wxColour settingsColor = wxColour(244, 98, 66);

	//Objects
	MathList<MathFrame> *modes;
	wxGridBagSizer *simplySizer, *expandSizer, *compareSizer, *settingsSizer;
	MathInput *simplyInput, *expandInput, *compInput1, *compInput2;
	History *simplyHistory, *expandHistory, *compHistory1, *compHistory2;
	wxPanel *bottom, *settingsPanel;

	std::size_t currentStatus = 0;
	CalcMode currentMode = CalcMode::Simply;

	//Frame Events
	void SliderEvent(wxScrollEvent &event) {
		inputHeight = event.GetPosition();
		auto *textHolder = settingsPanel->GetWindowChild(settings_sldtxt);
		auto label = textHolder->GetLabel();
		label.erase(label.begin() + label.find_last_of('(') + 1, label.end());
		label.append(std::to_string(inputHeight) + ")");
		textHolder->SetLabel(label);

		wxSize size(1, inputHeight);
		simplyInput->SetMinSize(size);
		expandInput->SetMinSize(size);
		compInput1->SetMinSize(size);
		compInput2->SetMinSize(size);
	}

	void ColorEvent(wxColourPickerEvent &event) {
		settingsColor = event.GetColour();
		settingsPanel->SetBackgroundColour(settingsColor);
		settingsPanel->Refresh();
	}

	void GlobalFontEvent(wxFontPickerEvent &event) {
		globalFont = event.GetFont();

		modes->SetFont(globalFont);
		settingsPanel->GetWindowChild(settings_clrtxt)->SetFont(globalFont);
		settingsPanel->GetWindowChild(settings_fonttxt)->SetFont(globalFont);
		settingsPanel->GetWindowChild(settings_fontItxt)->SetFont(globalFont);
		settingsPanel->GetWindowChild(settings_sldtxt)->SetFont(globalFont);
		bottom->GetWindowChild(button_calc)->SetFont(globalFont);
		bottom->GetWindowChild(button_clearInput)->SetFont(globalFont);
		bottom->GetWindowChild(button_clearHistory)->SetFont(globalFont);
		GetWindowChild(comp_panel)->GetWindowChild(comp_result)->SetFont(globalFont);
		GetWindowChild(comp_panel)->GetWindowChild(button_calc)->SetFont(globalFont);
		GetWindowChild(comp_panel)->GetWindowChild(button_clearHistory)->SetFont(globalFont);

		Layout();
		Refresh();
	}

	void InputFontEvent(wxFontPickerEvent &event) {
		inputFont = event.GetFont();

		simplyInput->SetFont(inputFont);
		expandInput->SetFont(inputFont);
		compInput1->SetFont(inputFont);
		compInput2->SetFont(inputFont);

		simplyInput->UpdateInput();
		expandInput->UpdateInput();
		compInput1->UpdateInput();
		compInput2->UpdateInput();

		Layout();
		Refresh();
	}

	void CalcButton(wxCommandEvent &event = wxCommandEvent()) {
		MathInput *input = NULL;
		switch (currentMode) {
		case CalcMode::Simply:
			input = simplyInput;
			break;
		case CalcMode::Expand:
			input = expandInput;
			break;
		case CalcMode::Compare:
			if (compInput1->output != NULL && compInput2->output != NULL && !compInput1->content.empty() && !compInput2->content.empty()) {
				std::string builder1(compInput1->content.begin(), compInput1->content.end());
				std::string builder2(compInput2->content.begin(), compInput2->content.end());

				std::size_t lvlCopy1 = compInput1->currentLvl;
				std::size_t lvlCopy2 = compInput2->currentLvl;
				while (lvlCopy1 != 0 && lvlCopy1-- >= 0)
					builder1.insert(builder1.end(), MathInput::powEnd.begin(), MathInput::powEnd.end());
				while (lvlCopy2 != 0 && lvlCopy2-- >= 0)
					builder2.insert(builder2.end(), MathInput::powEnd.begin(), MathInput::powEnd.end());

				std::string parsed1 = readHTML(builder1), parsed2 = readHTML(builder2);

				Expresion primary = Simplifier(getAsociated(parsed1)).getResult(),
					secondary = Simplifier(getAsociated(parsed2)).getResult();

				if (primary == secondary)
					GetWindowChild(comp_result)->SetBackgroundColour(m_Colors::success);
				else
					GetWindowChild(comp_result)->SetBackgroundColour(m_Colors::error);
				Refresh();

				compInput1->output->Log(parsed1);
				compInput2->output->Log(parsed2);

				compInput1->Clear();
				compInput2->Clear();
				compInput1->shouldClear = true;
				compInput2->shouldClear = true;
			}
			return;
		}

		if (input->output != NULL) {
			std::string builder(input->content.begin(), input->content.end());

			std::size_t lvlCopy = input->currentLvl;
			while (lvlCopy != 0 && lvlCopy-- >= 0)
				builder.insert(builder.end(), MathInput::powEnd.begin(), MathInput::powEnd.end());

			input->output->Log(calculate(readHTML(builder), currentMode));
			input->Clear();
		}
	}

	void ClearInputButton(wxCommandEvent &event) {
		switch (currentMode) {
		case CalcMode::Simply:
			simplyInput->Clear();
			break;
		case CalcMode::Expand:
			expandInput->Clear();
			break;
		}
	}

	void ClearHistoryButton(wxCommandEvent &event) {
		switch (currentMode) {
		case CalcMode::Simply:
			simplyHistory->ClearAll();
			break;
		case CalcMode::Expand:
			expandHistory->ClearAll();
			break;
		case CalcMode::Compare:
			compHistory1->ClearAll();
			compHistory2->ClearAll();
			break;
		}
	}

	void ModeSelect(wxListEvent &event) {
		GetSizer()->Detach(modes);
		GetSizer()->Detach(bottom);
		switch (event.GetItem()) {
		case 0:
			currentMode = CalcMode::Simply;
			simplySizer->Add(modes, wxGBPosition(0, 0), wxGBSpan(3, 1), wxEXPAND);
			simplySizer->Add(bottom, wxGBPosition(2, 1), wxDefaultSpan, wxEXPAND);
			simplyHistory->Show();
			bottom->Show();
			simplyInput->Show();
			SetSizer(simplySizer, false);
			switch (currentStatus) {
			case 1:
				expandHistory->Hide();
				expandInput->Hide();
				break;
			case 2:
				compInput1->Hide();
				compInput2->Hide();
				compHistory1->Hide();
				compHistory2->Hide();
				GetWindowChild(comp_panel)->Hide();
				break;
			case 3:
				settingsPanel->Hide();
				break;
			}
			break;
		case 1:
			currentMode = CalcMode::Expand;
			expandSizer->Add(modes, wxGBPosition(0, 0), wxGBSpan(3, 1), wxEXPAND);
			expandSizer->Add(bottom, wxGBPosition(2, 1), wxDefaultSpan, wxEXPAND);
			expandHistory->Show();
			bottom->Show();
			expandInput->Show();
			SetSizer(expandSizer, false);
			switch (currentStatus) {
			case 0:
				simplyHistory->Hide();
				simplyInput->Hide();
				break;
			case 2:
				compInput1->Hide();
				compInput2->Hide();
				compHistory1->Hide();
				compHistory2->Hide();
				GetWindowChild(comp_panel)->Hide();
				break;
			case 3:
				settingsPanel->Hide();
				break;
			}
			break;
		case 2:
			currentMode = CalcMode::Compare;
			compareSizer->Add(modes, wxGBPosition(0, 0), wxGBSpan(5, 1), wxEXPAND);
			compInput1->Show();
			compInput2->Show();
			compHistory1->Show();
			compHistory2->Show();
			GetWindowChild(comp_panel)->Show();
			SetSizer(compareSizer, false);
			switch (currentStatus) {
			case 0:
				simplyHistory->Hide();
				simplyInput->Hide();
				GetWindowChild(bottom_panel)->Hide();
				break;
			case 1:
				expandHistory->Hide();
				expandInput->Hide();
				GetWindowChild(bottom_panel)->Hide();
				break;
			case 3:
				settingsPanel->Hide();
				break;
			}
			break;
		case 3:
			currentMode = CalcMode::Other;
			settingsSizer->Add(modes, wxGBPosition(0, 0), wxGBSpan(5, 1), wxEXPAND);
			settingsPanel->Show();
			SetSizer(settingsSizer, false);
			switch (currentStatus) {
			case 0:
				simplyHistory->Hide();
				simplyInput->Hide();
				GetWindowChild(bottom_panel)->Hide();
				break;
			case 1:
				expandHistory->Hide();
				expandInput->Hide();
				GetWindowChild(bottom_panel)->Hide();
				break;
			case 2:
				compInput1->Hide();
				compInput2->Hide();
				compHistory1->Hide();
				compHistory2->Hide();
				GetWindowChild(comp_panel)->Hide();
				break;
			}
			break;
		}

		currentStatus = event.GetItem();
		Layout();
		Refresh();
	}

public:
	MathFrame(const string &name, vector<Property> properties);

	void OnClose(wxCloseEvent &event) {
		setProperty("Simply.Input", readHTML(simplyInput->content));
		setProperty("Expand.Input", readHTML(expandInput->content));
		setProperty("Compare.Input.Primary", readHTML(compInput1->content));
		setProperty("Compare.Input.Secondary", readHTML(compInput2->content));


		vector<string> simplyItems;
		for (std::size_t i = 0; i < simplyHistory->GetItemCount(); i++)
			simplyItems.emplace_back(simplyHistory->GetItemText(i));
		setProperty("Simply.History", simplyItems);
		vector<string> expandItems;
		for (std::size_t i = 0; i < expandHistory->GetItemCount(); i++)
			expandItems.emplace_back(expandHistory->GetItemText(i));
		setProperty("Expand.History", expandItems);
		vector<string> compItems1;
		for (std::size_t i = 0; i < compHistory1->GetItemCount(); i++)
			compItems1.emplace_back(compHistory1->GetItemText(i));
		setProperty("Compare.History.Primary", compItems1);
		vector<string> compItems2;
		for (std::size_t i = 0; i < compHistory2->GetItemCount(); i++)
			compItems2.emplace_back(compHistory2->GetItemText(i));
		setProperty("Compare.History.Secondary", compItems2);

		setProperty("Settings.Height", inputHeight);
		std::vector<std::size_t> colors(3);
		colors[0] = settingsColor.Red();
		colors[1] = settingsColor.Green();
		colors[2] = settingsColor.Blue();
		setProperty("Settings.Color", colors);
		setProperty("Settings.Font.Global.Face", globalFont.GetFaceName().ToStdString());
		setProperty("Settings.Font.Global.Size", globalFont.GetPointSize());
		setProperty("Settings.Font.Input.Face", inputFont.GetFaceName().ToStdString());
		setProperty("Settings.Font.Input.Size", inputFont.GetPointSize());
		Destroy();
	}

	void buildSimplify();
	void buildExpand();
	void buildCompare();
	void buildSettings();
};

wxIMPLEMENT_APP(MathHope);

#endif