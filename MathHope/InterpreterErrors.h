#ifndef _InterpreterErrors
#define _InterpreterErrors
#include <iostream>
using namespace std;

class Interpreter;

namespace m_Interpreter {

	class BracketContainer;

	enum ErrorType {
		AllClosed,
		AlreadyClosed,
		UnespectedBracket,
		InternalError
	};

	class interpreter_error : public runtime_error {
		friend class ::Interpreter;

		ErrorType ie_type;
		BracketContainer *ie_errored;
	
		const char* getMessage(ErrorType type) {
			switch (type) {
			case AllClosed:
				return "Attempting to close a separator, but already closed all";
			case AlreadyClosed:
				return "Attempting to close an already closed separator";
			case UnespectedBracket:
				return "Spected one bracket but got another. Check syntax";
			case InternalError:
				return "Didn't found an open separator, check data flush";
			default:
				return "Unknown Error, not sure what u did to be honest";
			}
		}

		BracketContainer m_getContainer();
		
	public:
		interpreter_error(ErrorType type, BracketContainer *container) : ie_type(type), ie_errored(container), runtime_error(getMessage(type)) {
		}

		const ErrorType &getType() {
			return ie_type;
		}

		const BracketContainer &getContainer();
	};

}
#endif