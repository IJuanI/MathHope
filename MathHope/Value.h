#ifndef _MathValue
#define _MathValue

#include "Unknown.h"
#include <memory>

using namespace Utils;

struct Value {
private:
	bool m_isComplex = false, m_isKnown = true, m_isConstant = true;
	friend class Component;
	Value &operator=(Value &other);
public:

	//Singletons
	static const Value &ZERO() { static Value zero(0); return zero; }
	static const Value &ONE() { static Value one(1); return one; }
	static const Value &TWO() { static Value two(2); return two; }

	//Actual code
	std::unique_ptr<Expresion> complexValue;
	std::unique_ptr<Unknown> v_unknown;
	std::unique_ptr<size_t> healtyValue;

	Value(const Value& other);
	Value(const size_t &value);
	Value(const Unknown &value);
	Value(const Expresion &value);

	bool operator==(const Value &other) const;
	bool operator==(const Unknown &unknown) const;
	bool operator!=(const Value &other) const;
	std::shared_ptr<Value> operator^(const Value &value) const;
	std::shared_ptr<Value> operator*(size_t value) const;
	std::shared_ptr<Value> operator*(const Value &value) const;
	std::shared_ptr<Value> operator+(const Value &value) const;
	std::shared_ptr<Value> operator+(size_t value) const;

	bool isComplex() const {
		return m_isComplex;
	}

	bool isKnown() const {
		return !m_isComplex && m_isKnown;
	}

	bool isConstant() const {
		return m_isConstant;
	}

	void setConstant(bool constant) {
		m_isConstant = constant;
	}

	std::vector<char> toString() const;
	std::string toHTMLString(std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const;
};

#endif