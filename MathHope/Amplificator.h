#ifndef _MathAmplificator
#define _MathAmplificator
#include "Expresion.h"

class Amplificator {
	Expresion a_result;
	Expresion amplificate(MathTerm &term);

public:
	Amplificator(const Expresion &expresion);

	Expresion &getResult();
};

#endif