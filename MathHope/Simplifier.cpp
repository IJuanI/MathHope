#include "Simplifier.h"
#include "Interpreter.h"

using namespace Operations;
using namespace std;

Expresion Simplifier::checkChilds(const Expresion &expresion) const {
	Expresion result;

	vector<MathTerm> &terms = expresion.getTerms();

	for (MathTerm &term : terms)
		for (Component &comp : term.t_components) {
			if (comp.hasExponent() && comp.getExponent().isComplex()) {
				Expresion &res = Simplifier(*comp.getExponent().complexValue).getResult();
				if (res.m_count != 0 && !(res == *comp.getExponent().complexValue))
					comp.m_exponent = make_shared<Value>(res);
			}

			if (comp.getValue().isComplex()) {
				Expresion res = Simplifier(*comp.getValue().complexValue).getResult();
				if (res.m_count != 0 && !(res == *comp.getValue().complexValue))
					comp.m_val = make_shared<Value>(res);
			}
		}

	for (MathTerm &term : terms)
		result += term;

	return result;
}

vector<size_t> Simplifier::getPossibilities(const vector<size_t> &list, size_t val) {
	vector<size_t> result;
	for (size_t i = 0; i < list.size(); i++) {
		vector<size_t> sub;
		for (size_t j = 0; j < list.size() - 1; j++)
			if (i != j)
				sub.push_back(list[j]);
		vector<size_t> &res = getPossibilities(sub, val == 0 ? list[i] : val * list[i]);
		result.insert(result.end(), res.begin(), res.end());
	}

	if (val != 0)
		result.emplace_back(val);

	return result;
}

Simplifier::Simplifier(const Expresion &expresion) {
	s_result = checkChilds(expresion);
	s_result = getCommon(s_result);
	s_result = checkChilds(s_result);

	Expresion expandFix;
	for (MathTerm &term : s_result.m_terms)
		if (!term.isKnown()) {
			size_t coeff = 1;
			auto iter = find(term.t_components, (Func<Component>) [](Component comp) { return comp.isCompactable(); });
			if (iter != term.t_components.end())
				coeff = *iter->getCompact().healtyValue;

			if (coeff != 0)
				if (coeff == 1)
					expandFix += term;
				else {
					term >> *iter;
					for (size_t i = 0; i < coeff; i++)
						expandFix += MathTerm(term);
				}
		} else
			expandFix += term;

	s_result = expandFix;

	if (s_result.m_count != 1 && !isPrime(s_result.m_count)) {
		vector<size_t> primeFactors;
		size_t val = s_result.m_count;

		for (size_t &prime : getPrimesWithMax(val))
			while (val % prime == 0) {
				primeFactors.push_back(prime);
				val /= prime;
			}

		vector<size_t> &divisors = getPossibilities(primeFactors);
		set<size_t> sorter;
		for (size_t &div : divisors)
			sorter.insert(div);
		divisors.assign(sorter.begin(), sorter.end());
		reverse(divisors.begin() , divisors.end());

		for (size_t divisor : divisors) {
			Expresion tempResult = getCommonByGroup(s_result, divisor);
			if (tempResult.m_count != 0) {
				tempResult = checkChilds(tempResult);
				std::string dummy = Interpreter(tempResult).getText();
				Expresion &tempCommon = getCommon(tempResult);
				if (tempCommon != tempResult) {
					s_result = checkChilds(tempCommon);
					break;
				}
			}
		}
	}

	for (MathTerm &term : s_result.m_terms)
		for (Component &comp : term.getComponents()) {
			if (!comp.hasExponent() && comp.m_val->isComplex() && comp.m_val->complexValue->m_count == 1) {
				for (Component &c : (*comp.m_val->complexValue)[0].t_components)
					term << c;
				term >> comp;
			}
		}

	if (s_result.m_count == 1 && s_result[0].t_count == 1 && !s_result[0][0].hasExponent() && s_result[0][0].getValue().isComplex())
		s_result = *s_result[0][0].getValue().complexValue;
}

Expresion &Simplifier::getResult() {
	return s_result;
}