#include "Expresion.h"

Expresion::Expresion(const MathTerm &term) {
	operator+=(term);
}

Expresion &Expresion::operator+=(const Expresion &expresion) {
	size_t newCount = m_count + expresion.m_count;

	if (newCount >= m_terms.size())
		m_terms.resize(newCount);

	for (size_t i = 0; i < expresion.m_count; i++)
		m_terms[m_count + i] = expresion.m_terms[i];

	m_count = newCount;

	return *this;
}

Expresion &Expresion::operator+=(const MathTerm &term) {
	/*if (term.isEmpty())
		return *this;*/

	if (m_count >= m_terms.size())
		m_terms.resize(m_count + 1);

	m_terms[m_count] = term;

	m_count++;

	return *this;
}

Expresion Expresion::operator+(const Value &value) const {
	Expresion expresion;
	std::vector<MathTerm> allTerms;
	Component free;
	allTerms.insert(allTerms.end(), m_terms.begin(), m_terms.end());

	if (value.isComplex()) {
		std::vector<MathTerm> terms = (*value.complexValue).m_terms;
		allTerms.insert(allTerms.end(), terms.begin(), terms.end());
	} else
		allTerms.emplace_back(MathTerm(Component(value)));

	for (MathTerm term : allTerms) {
		term.compactate();
		
		if (term.isKnown() && term.isCompact())
			free *= term[0];
		else {
			size_t index = expresion.index(term);
			if (index == -1)
				expresion += term;
			else
				term[index] *= 2;
		}
	}

	expresion += MathTerm(free);

	return expresion;
}

Expresion &Expresion::operator-=(const MathTerm &term) {
	bool remove = false;
	for (size_t i = 0; i < m_count; i++)
		if (!remove && m_terms.at(i) == term)
			remove = true;
		else if (remove)
			m_terms[i - 1] = m_terms[i];
	m_count--;

	return *this;
}

MathTerm &Expresion::operator[](size_t index) {
	return m_terms.at(index);
}

bool Expresion::operator==(const Expresion &expresion) const {
	std::vector<MathTerm> pendient = m_terms;

	for (MathTerm term : expresion.m_terms) {
		auto iter = find(pendient, (Func<MathTerm>) [&term](MathTerm other) { return term.unsortedEqual(other); });
		if (iter == pendient.end())
			return false;
		else
			pendient.erase(iter);
	}

	return pendient.empty();
}

Expresion &Expresion::operator*=(Component &comp) {
	for (MathTerm& term : m_terms)
		term += comp;
	return *this;
}

Expresion Expresion::operator*(const Expresion &expresion) const {
	Expresion result;
	for (MathTerm &left : getTerms())
		for (MathTerm &right : expresion.getTerms()) 
			result += left * right;
	return result;
}

Expresion Expresion::operator*(size_t value) const {
	Expresion base;

	for (MathTerm term : m_terms)
		base += term * value;

	return base;
}

std::vector<MathTerm> Expresion::getTerms() const {
	return std::vector<MathTerm>(m_terms);
}

size_t Expresion::size() {
	return m_terms.size();
}

bool Expresion::isMember(const MathTerm &term) const {
	return findConst(m_terms, term) != m_terms.end();
}

ptrdiff_t Expresion::index(const MathTerm &term) const {
	auto iter = findConst(m_terms, term);
	if (iter == m_terms.end())
		return -1;
	return std::distance(m_terms.begin(), iter);

}

ptrdiff_t Expresion::index(const Value &value) const {
	auto iter = findConst(m_terms, (Func<MathTerm>) [&value](MathTerm term)
		{ return term.t_count == 1 && !term[0].hasExponent() && term[0].getValue() == value; });
	if (iter == m_terms.end())
		return -1;
	return std::distance(m_terms.begin(), iter);
}

void Expresion::clear() {
	m_terms.clear();
	m_count = 0;
}

std::vector<char> Expresion::toString() const {
	std::vector<char> result;
	bool child = true;
	for (MathTerm term : m_terms) {
		std::vector<char> parsedTerm = term.toString(child);
		child = false;
		result.insert(result.end(), parsedTerm.begin(), parsedTerm.end());
	}

	return result;
}

std::string Expresion::toHTMLString(std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const {
	std::string result;
	bool child = true;
	for (MathTerm term : m_terms) {
		std::string parsedTerm = term.toHTMLString(child, result.size() + overall, powIndexes);
		child = false;
		result.insert(result.end(), parsedTerm.begin(), parsedTerm.end());
	}

	return result;
}
