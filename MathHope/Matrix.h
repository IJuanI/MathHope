#include <vector>

template <typename T>
class Matrix {
	std::vector<std::vector<T>> data;
public:
	Matrix(std::size_t x, std::size_t y, T def = T()) {
		for (size_t i = 0; i < x; i++)
			data.emplace_back(y, def);
	}

	std::vector<T> &operator[](std::size_t index) {
		return data[index];
	}
};