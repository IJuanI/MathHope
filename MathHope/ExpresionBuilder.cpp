#include "Expresion.h"
#include "ExpresionBuilder.h"

namespace Utils {
	namespace Values {
		std::shared_ptr<Value> directSum(const Value &val1, const Value &val2) {
			Expresion expresion;

			expresion += MathTerm(Component(val1));
			expresion += MathTerm(Component(val2));

			return std::make_shared<Value>(expresion);
		}
	}

	namespace Terms {
		template<typename ...Args>
		MathTerm multiply(const Value &val1, const Value &val2, const Args... more) {
			std::array<Value, sizeof...(Args)+2> const values{ val1, val2, more... };

			MathTerm term;
			Component &free = Component();

			for (Value val : values)
				if (val.isKnown())
					free *= *val.healtyValue;
				else {
					int index = term.index(val);
					if (index == -1)
						term += Component(val);
					else
						term[index].sumToExponent(1);
				}
				term += free;
				return term;
		}
	}

	namespace Expresions {
		Expresion getExpresion(Component &component) {
			return getExpresion(MathTerm() += component);
		}

		Expresion getExpresion(const MathTerm &term) {
			return Expresion() += term;
		}

		Expresion multiply(const MathTerm &base, const std::vector<MathTerm> &multipliers) {
			if (multipliers.size() == 0)
				return Expresion(MathTerm(base));
			else {
				Expresion result;

				for (MathTerm term : multipliers)
					result += base * term;

				return result;
			}
		}

		template<typename ...Args>
		Expresion multiply(const Value &val1, const Value &val2, const Args... more) {
			return Expresion(Terms::multiply(val1, val2, more...));
		}

		template<typename ...Args>
		Expresion &sumUp(const Value &val1, const Value &val2, const Args... more) {
			std::array<Value, sizeof...(Args)+2> const values{ val1, val2, more... };

			Expresion result;
			MathTerm &free = MathTerm();

			for (Value val : values)
				if (val.isKnown())
					free.add(val);
				else {
					int index = result.index(val);
					if (index == -1)
						result += MathTerm(Component(val));
					else
						result[index].add(val);
				}

				result += free;

				return result;
		}
	}
}