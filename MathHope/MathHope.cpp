#include "MathHope.h"

namespace m_MathHope {
	bool m_testMode = false;
	bool &testMode() {
		return m_testMode;
	}
}

const std::string MathInput::header = "<html><body><p><font face=\"";
const std::string MathInput::footer = "</font></p></body></html>";
const std::string MathInput::powStart = "<sup>";
const std::string MathInput::powEnd = "</sup>";
const std::string MathInput::boldStart = "<b>";
const std::string MathInput::boldEnd = "</b>";

Expresion getAsociated(const std::string input) {
	Expresion parsed = Interpreter(input).getExpresion();
	std::string dummy1 = Interpreter(parsed).getText();
	Expresion amplified = Amplificator(parsed).getResult();
	std::string dummy2 = Interpreter(amplified).getText();
	Expresion asociated = Asociator(amplified).getResult();
	std::string dummy3 = Interpreter(asociated).getText();
	return asociated;
}

wxString calculate(const std::string &input, CalcMode mode) {
	switch (mode) {
	case CalcMode::Simply:
		return wxString(Interpreter(Asociator(Simplifier(getAsociated(input)).getResult()).getResult()).getText());
	case CalcMode::Expand:
		return wxString(Interpreter(getAsociated(input)).getText());
	}

	return wxString(input);
}

bool compare(const std::string &primary, const std::string &secondary) {
	Expresion &prim_simply = Simplifier(getAsociated(primary)).getResult();
	Expresion &sec_simply = Simplifier(getAsociated(secondary)).getResult();

	return prim_simply == sec_simply;
}

std::string readHTML(const std::string input) {
	std::string parsed = input;
	std::string::size_type offset = 0;
	while ((offset = parsed.find(MathInput::powStart, offset)) != std::string::npos) {
		parsed.replace(offset, MathInput::powStart.size(), "^(");
		offset++;
	}

	offset = 0;
	while ((offset = parsed.find(MathInput::powEnd, offset)) != std::string::npos) {
		parsed.replace(offset, MathInput::powEnd.size(), ")");
		offset++;
	}

	return parsed;
}

//Wx Stuff Starts from Here

//Register Events
wxBEGIN_EVENT_TABLE(MathList<MathFrame>, wxListCtrl)
	EVT_LIST_ITEM_SELECTED(m_ListCTRL, MathList<MathFrame>::OnItemSelected)
	EVT_LIST_ITEM_RIGHT_CLICK(m_ListCTRL, MathList<MathFrame>::OnItemRightClick)
wxEND_EVENT_TABLE()

wxBEGIN_EVENT_TABLE(History, wxListCtrl)
	EVT_LIST_ITEM_RIGHT_CLICK(simp_History, History::OnRightClick)
	EVT_LIST_ITEM_RIGHT_CLICK(exp_History, History::OnRightClick)
	EVT_LIST_ITEM_RIGHT_CLICK(comp_History, History::OnRightClick)
	EVT_LIST_ITEM_RIGHT_CLICK(comp_History2, History::OnRightClick)
wxEND_EVENT_TABLE()

bool MathHope::OnInit() {
	props.Load();

	vector<Property> properties = vector<Property>{
		getProperty("Simply.Input"), getProperty("Simply.History"),
		getProperty("Expand.Input"), getProperty("Expand.History"),
		getProperty("Compare.Input.Primary") , getProperty("Compare.Input.Secondary"),
		getProperty("Compare.History.Primary"), getProperty("Compare.History.Secondary"),
		getProperty("Settings.Height"), getProperty("Settings.Color"),
		getProperty("Settings.Font.Global.Face"), getProperty("Settings.Font.Global.Size"),
		getProperty("Settings.Font.Input.Face"), getProperty("Settings.Font.Input.Size") };

	MathFrame *frame = new MathFrame("MathHope", properties);
	frame->Show(true);
	return true;
}

int MathHope::OnExit() {
	props.Save();
	return 0;
}

MathFrame::MathFrame(const string &name, vector<Property> properties) : wxFrame(NULL, wxID_ANY, name, wxDefaultPosition, wxSize(480, 300)) {
	Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MathFrame::OnClose), NULL, this);
	
	for (size_t i = 0; i < properties.size(); i++) {
		Property prop = properties[i];
		if (!prop.isEmpty())
			switch (i) {
			case 8:
				inputHeight = prop.get<size_t>();
				break;
			case 9: {
				vector<size_t> colors = prop.get<vector<size_t>>();
				settingsColor = wxColour(colors[0], colors[1], colors[2]);
			}
				break;
			case 10:
				globalFont.SetFaceName(prop.get<string>());
				break;
			case 11:
				globalFont.SetPointSize(prop.get<size_t>());
				break;
			case 12:
				inputFont.SetFaceName(prop.get<string>());
				break;
			case 13:
				inputFont.SetPointSize(prop.get<size_t>());
				break;
			}
	}

	SetMinSize(wxSize(385, 300));
	//Left Panel
	modes = new MathList<MathFrame>(this, m_ListCTRL, this, &MathFrame::ModeSelect);
	modes->Fill(4, "Simplificate", "Expand", "Check equality", "Settings");
	modes->SetFont(globalFont);

	//Initialize Contents
	buildSimplify();
	buildExpand();
	buildCompare();
	buildSettings();

	//Bottom Buttons
	bottom = new wxPanel(this, bottom_panel);
	wxBoxSizer *bottomSizer = new wxBoxSizer(wxHORIZONTAL);

	wxButton *calcButton = new wxButton(bottom, button_calc, "Calculate");
	wxButton *clearIButton = new wxButton(bottom, button_clearInput, "Clear Input");
	wxButton *clearHistButton = new wxButton(bottom, button_clearHistory, "Clear History");
	calcButton->SetFont(globalFont);
	clearIButton->SetFont(globalFont);
	clearHistButton->SetFont(globalFont);
	bottomSizer->Add(calcButton, 1);
	bottomSizer->Add(clearIButton, 1);
	bottomSizer->Add(clearHistButton, 1);

	Connect(button_calc, wxEVT_BUTTON, wxCommandEventHandler(MathFrame::CalcButton));
	Connect(button_clearInput, wxEVT_BUTTON, wxCommandEventHandler(MathFrame::ClearInputButton));
	Connect(button_clearHistory, wxEVT_BUTTON, wxCommandEventHandler(MathFrame::ClearHistoryButton));

	bottom->SetSizer(bottomSizer);

	//Set Simplify as default
	simplySizer->Add(modes, wxGBPosition(0, 0), wxGBSpan(3, 1), wxEXPAND);
	simplySizer->Add(bottom, wxGBPosition(2, 1), wxDefaultSpan, wxEXPAND);
	SetSizer(simplySizer);

	SetBackgroundColour(wxColour(0, 0, 0));

	for (size_t i = 0; i < properties.size(); i++) {
		Property prop = properties[i];
		if (!prop.isEmpty())
			switch (i) {
			case 0:
				simplyInput->SetFromHistory(prop.get<string>());
				break;
			case 1:
				for (string line : prop.get<vector<string>>())
					simplyHistory->Log(line);
				break;
			case 2:
				expandInput->SetFromHistory(prop.get<string>());
				break;
			case 3:
				for (string line : prop.get<vector<string>>())
					expandHistory->Log(line);
				break;
			case 4:
				compInput1->SetFromHistory(prop.get<string>());
				break;
			case 5:
				compInput2->SetFromHistory(prop.get<string>());
				break;
			case 6:
				for (string line : prop.get<vector<string>>())
					compHistory1->Log(line);
				break;
			case 7:
				for (string line : prop.get<vector<string>>())
					compHistory2->Log(line);
				break;
			}
	}
}

void MathFrame::buildSimplify() {
	simplySizer = new wxGridBagSizer();

	//Create text input
	simplyInput = new MathInput(this, CalcMode::Simply, simp_input);
	simplyInput->SetMinSize(wxSize(1, inputHeight));
	simplyInput->SetFont(inputFont);
	simplySizer->Add(simplyInput, wxGBPosition(0, 1), wxDefaultSpan, wxEXPAND | wxALIGN_CENTRE_VERTICAL);

	//Create history
	simplyHistory = new History(this, simplyInput, simp_History);
	simplyHistory->SetBackgroundColour(m_Colors::simply);
	simplyInput->SetOutput(simplyHistory);
	simplySizer->Add(simplyHistory, wxGBPosition(1, 1), wxDefaultSpan, wxEXPAND);

	//Adjust Display
	simplySizer->SetVGap(1);
	simplySizer->SetHGap(1);
	simplySizer->AddGrowableCol(0, 1);
	simplySizer->AddGrowableCol(1, 6);
	simplySizer->AddGrowableRow(1, 1);
}

void MathFrame::buildExpand() {
	expandSizer = new wxGridBagSizer();

	//Create text input
	expandInput = new MathInput(this, CalcMode::Expand, exp_input);
	expandInput->SetMinSize(wxSize(1, inputHeight));
	expandInput->SetFont(inputFont);
	expandInput->Hide();
	expandSizer->Add(expandInput, wxGBPosition(0, 1), wxDefaultSpan, wxEXPAND | wxALIGN_CENTRE_VERTICAL);

	//Create history
	expandHistory = new History(this, expandInput, exp_History);
	expandHistory->SetBackgroundColour(m_Colors::expand);
	expandInput->SetOutput(expandHistory);
	expandHistory->Hide();
	expandSizer->Add(expandHistory, wxGBPosition(1, 1), wxDefaultSpan, wxEXPAND);
	
	//Adjust Display
	expandSizer->SetVGap(1);
	expandSizer->SetHGap(1);
	expandSizer->AddGrowableCol(0, 1);
	expandSizer->AddGrowableCol(1, 6);
	expandSizer->AddGrowableRow(1, 1);
}

void MathFrame::buildCompare() {
	compareSizer = new wxGridBagSizer();

	//Create equation inputs
	compInput1 = new MathInput(this, CalcMode::Compare, comp_input);
	compInput1->SetMinSize(wxSize(1, inputHeight));
	compInput1->SetFont(inputFont);
	compInput1->Hide();
	compareSizer->Add(compInput1, wxGBPosition(0, 1), wxDefaultSpan, wxEXPAND | wxALIGN_CENTRE_VERTICAL);

	compInput2 = new MathInput(this, CalcMode::Compare, comp_input2);
	compInput2->SetMinSize(wxSize(1, inputHeight));
	compInput2->SetFont(inputFont);
	compInput2->Hide();
	compareSizer->Add(compInput2, wxGBPosition(3, 1), wxDefaultSpan, wxEXPAND | wxALIGN_CENTRE_VERTICAL);

	//Create historials
	compHistory1 = new History(this, compInput1, comp_History);
	compHistory1->SetBackgroundColour(m_Colors::compare);
	compInput1->SetOutput(compHistory1);
	compHistory1->Hide();
	compareSizer->Add(compHistory1, wxGBPosition(1, 1), wxDefaultSpan, wxEXPAND);

	compHistory2 = new History(this, compInput2, comp_History2);
	compHistory2->SetBackgroundColour(m_Colors::compare);
	compInput2->SetOutput(compHistory2);
	compHistory2->Hide();
	compareSizer->Add(compHistory2, wxGBPosition(4, 1), wxDefaultSpan, wxEXPAND);


	//Middle Buttons
	wxPanel *middle = new wxPanel(this, comp_panel);
	wxBoxSizer *middleSizer = new wxBoxSizer(wxHORIZONTAL);

	wxTextCtrl *result = new wxTextCtrl(middle, comp_result, "Result",
		wxDefaultPosition, wxSize(1, 1), wxTE_CENTRE | wxTE_READONLY);
	result->SetBackgroundColour(m_Colors::def);
	result->SetFont(globalFont);

	wxButton *calcButton = new wxButton(middle, button_calc, "Calculate");
	wxButton *clearButton = new wxButton(middle, button_clearHistory, "Clear History");
	calcButton->SetFont(globalFont);
	clearButton->SetFont(globalFont);
	middleSizer->Add(calcButton, 2, wxEXPAND);
	middleSizer->Add(result, 1, wxEXPAND);
	middleSizer->Add(clearButton, 2, wxEXPAND);
	middle->SetSizer(middleSizer);
	middle->Hide();
	compareSizer->Add(middle, wxGBPosition(2, 1), wxDefaultSpan, wxEXPAND);

	//Adjust Display
	compareSizer->SetVGap(1);
	compareSizer->SetHGap(1);
	compareSizer->AddGrowableCol(0, 1);
	compareSizer->AddGrowableCol(1, 6);
	compareSizer->AddGrowableRow(1, 10);
	compareSizer->AddGrowableRow(2, 1);
	compareSizer->AddGrowableRow(4, 10);
}

void MathFrame::buildSettings() {
	settingsSizer = new wxGridBagSizer();
	settingsPanel = new wxPanel(this, settings_panel);
	settingsPanel->SetBackgroundColour(settingsColor);
	wxBoxSizer *box = new wxBoxSizer(wxHORIZONTAL);
	wxGridBagSizer *sizer = new wxGridBagSizer();

	wxStaticText *iHText = new wxStaticText(settingsPanel, settings_sldtxt, wxString("Altura del panel de entrada (" + std::to_string(inputHeight) + ")"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	iHText->SetToolTip("Cambia la altura de todos los paneles donde se ingresan ecuaciones \n(En pixeles)");
	iHText->SetFont(globalFont);

	wxStaticText *fFText = new wxStaticText(settingsPanel, settings_fonttxt, wxT("Fuente global"));
	fFText->SetToolTip("Cambia el estilo de fuente usado para mostrar mensajes en la aplicacion");
	fFText->SetFont(globalFont);

	wxStaticText *fIText = new wxStaticText(settingsPanel, settings_fontItxt, wxT("Fuente de Ingreso"));
	fFText->SetToolTip("Cambia el estilo de fuente usado para registrar el ingreso de ecuaciones");
	fIText->SetFont(globalFont);

	wxStaticText *bCText = new wxStaticText(settingsPanel, settings_clrtxt, wxT("Color de fondo"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
	bCText->SetToolTip("Elige el color de fondo de este menu");
	bCText->SetFont(globalFont);

	wxFontPickerCtrl *fontPckr = new wxFontPickerCtrl(settingsPanel, settings_fontG, globalFont);
	wxFontPickerCtrl *fontIPckr = new wxFontPickerCtrl(settingsPanel, settings_fontI, inputFont);
	wxColourPickerCtrl *colorPckr = new wxColourPickerCtrl(settingsPanel, settings_color, settingsColor);

	sizer->Add(iHText, wxGBPosition(1, 0), wxDefaultSpan, wxEXPAND);
	sizer->Add(new wxSlider(settingsPanel, settings_slider, inputHeight, 20, 60), wxGBPosition(1, 1), wxGBSpan(1, 2), wxEXPAND);

	Connect(settings_slider, wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(MathFrame::SliderEvent));
	Connect(settings_color, wxEVT_COLOURPICKER_CHANGED, wxColourPickerEventHandler(MathFrame::ColorEvent));
	Connect(settings_fontG, wxEVT_FONTPICKER_CHANGED, wxFontPickerEventHandler(MathFrame::GlobalFontEvent));
	Connect(settings_fontI, wxEVT_FONTPICKER_CHANGED, wxFontPickerEventHandler(MathFrame::InputFontEvent));

	wxBoxSizer *centerV1 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *centerH1 = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *joint1 = new wxBoxSizer(wxVERTICAL);
	joint1->Add(fFText);
	joint1->Add(fontPckr);
	centerH1->Add(joint1, 0, wxALIGN_CENTER_VERTICAL);
	centerV1->Add(centerH1, 1, wxALIGN_CENTER_HORIZONTAL);
	sizer->Add(centerV1, wxGBPosition(2, 0), wxDefaultSpan, wxEXPAND);

	wxBoxSizer *centerV2 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *centerH2 = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *joint2 = new wxBoxSizer(wxVERTICAL);
	joint2->Add(fIText);
	joint2->Add(fontIPckr);
	centerH2->Add(joint2, 0, wxALIGN_CENTER_VERTICAL);
	centerV2->Add(centerH2, 1, wxALIGN_CENTER_HORIZONTAL);
	sizer->Add(centerV2, wxGBPosition(3, 0), wxDefaultSpan, wxEXPAND);

	wxBoxSizer *centerV3 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *centerH3 = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *joint3 = new wxBoxSizer(wxVERTICAL);
	joint3->Add(bCText);
	joint3->Add(colorPckr);
	centerH3->Add(joint3, 0, wxALIGN_CENTER_VERTICAL);
	centerV3->Add(centerH3, 1, wxALIGN_CENTER_HORIZONTAL);
	sizer->Add(centerV3, wxGBPosition(2, 2), wxGBSpan(2, 1), wxEXPAND);

	sizer->SetVGap(1);
	sizer->SetHGap(1);
	sizer->AddGrowableCol(0, 3);
	sizer->AddGrowableCol(1, 1);
	sizer->AddGrowableCol(2, 9);
	sizer->AddGrowableRow(0, 1);
	sizer->AddGrowableRow(1, 1);
	sizer->AddGrowableRow(3, 10);
	sizer->SetEmptyCellSize(wxSize(1, 1));

	box->Add(sizer, 1, wxALL | wxEXPAND, 15);
	settingsPanel->SetSizer(box);
	settingsPanel->Hide();
	settingsSizer->Add(settingsPanel, wxGBPosition(0, 1), wxGBSpan(5, 1), wxEXPAND);
	settingsSizer->AddGrowableCol(0, 1);
	settingsSizer->AddGrowableCol(1, 6);
	settingsSizer->AddGrowableRow(0, 1);
}