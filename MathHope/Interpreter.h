#ifndef __Interpreter
#define __Interpreter
#include "ExpresionContainer.h"
#include <string>
#include <algorithm>

class Interpreter {
	string i_raw;
	Expresion i_parsed;
	size_t i_current = 0;

	void i_init(string raw);

public:
	Interpreter(string raw);
	Interpreter(Expresion expresion);

	string getText();
	Expresion getExpresion();
};
#endif