#ifndef _MathAsociator
#define _MathAsociator
#include "Expresion.h"

class Asociator {

	Expresion as_result;

	MathTerm asociate(const MathTerm &term) const;

public:
	Asociator(const Expresion &expresion);

	Expresion &getResult();
};

#endif