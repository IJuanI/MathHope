#ifndef _MathUnknown
#define _MathUnknown
#include <cmath>
#include "Utils.h"
#include "ExpresionBuilder.h"

struct Value;

class Unknown {
private:
	char m_key;
	friend struct Value;
public:
	Unknown(char key = ' ') : m_key(key) {}

	bool operator==(const Unknown &unknown) const {
		return m_key == unknown.m_key;
	}

	bool operator==(char key) const {
		return m_key == key;
	}

	Expresion &operator*(size_t value) const;
	Expresion &operator^(size_t value) const;
};

Unknown getUnknown(char key);

#endif
