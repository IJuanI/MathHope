#include "Interpreter.h"

using namespace m_Interpreter;
using namespace std;

tuple<Bracket, bool> findByChar(char c) {
	Bracket bracket;
	bool open = false;
	switch (c) {
	case '{':
		open = true;
	case '}':
		bracket = BRACES;
		break;
	case '[':
		open = true;
	case ']':
		bracket = SQUARE;
		break;
	case '(':
		open = true;
	case ')':
		bracket = ROUND;
		break;
	default:
		bracket = NONE;
	}

	return make_tuple(bracket, open);
}

void Interpreter::i_init(string raw) {
	i_current = 0;
	if (i_raw != raw)
		i_raw = raw;

	BracketContainer bracketContainer(&i_current, raw);

	size_t size = raw.size();

	for (size_t i = 0; i < size; i++) {
		char c = raw.at(i);
		bool open;
		Bracket bracket;

		tie(bracket, open) = findByChar(c);

		if (bracket != NONE)
			if (open)
				bracketContainer.dig(i, bracket);
			else
				try {
					bracketContainer.end(i, bracket);
				} catch (interpreter_error error) {
					switch (error.getType()) {
					case UnespectedBracket:
						error.m_getContainer().b_content.close();
						i_current--;
						break;
					case AllClosed:
						throw runtime_error("Seems like you forgot to open a bracket");
					default:
						throw error;
					}
		}
	}

	if (!bracketContainer.checkDataIntegrity() || i_current != 0) {
		vector<string> solutions{ bracketContainer.removeErrors(raw), bracketContainer.fixErrorsLast(raw), bracketContainer.fixErrorsFirst(raw) };

		cout << "Seems like you had a mistake with expresion's brackets" << endl
			<< "Maybe you meant one of the following:" << endl
			<< "1. " << solutions[0].c_str() << endl
			<< "2. " << solutions[1].c_str() << endl
			<< "3. " << solutions[2].c_str() << endl
			<< "4. Other" << endl;

		string input;
		while (true) {
			getline(cin, input);
			input = input.substr(0, input.find_first_of(' '));

			if (!isdigit(input.at(0)))
				input = input.substr(1);

			switch (input.at(0)) {
			case '1':
			case '2':
			case '3':
				system("cls");
				bracketContainer.finalize();
				i_init(solutions[atoi(&input.at(0)) - 1]);
				break;
			case '4':
				system("cls");
				cout << "Insert an Expresion: ";
				getline(cin, input);
				bracketContainer.finalize();
				i_init(input);
				break;
			default:
				cout << "Invalid option, try again" << endl;
				continue;
			}

			break;
		}
	}

	ExpresionContainer expresionContainer(&bracketContainer);

	i_parsed = expresionContainer.analize(raw).e_content;
}

Interpreter::Interpreter(string raw) : i_raw(raw) {
	i_init(raw);
}

Interpreter::Interpreter(Expresion expresion) : i_parsed(expresion) {
	vector<char> &result = expresion.toString();

	i_raw = string(result.begin(), result.end());
}

string Interpreter::getText() {
	return i_raw;
}

Expresion Interpreter::getExpresion() {
	return i_parsed;
}