#ifndef _MathList
#define _MathList
#include "wx/listctrl.h"
#include <stdarg.h>

template<typename Class>
class MathList : public wxListCtrl {
	void(Class::*OnSelect)(wxListEvent&);
	void(Class::*OnRightClick)(wxListEvent&);
	Class *sourceClass = NULL;

public:
	MathList(wxWindow *parent, const wxWindowID id, long style = wxLC_LIST | wxLC_EDIT_LABELS)
		: wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize, style), OnSelect(NULL), OnRightClick(NULL) { }

	MathList(wxWindow *parent, const wxWindowID id, Class *sourceClass, void(Class::*OnSelect)(wxListEvent&),
		void(Class::*OnRightClick)(wxListEvent&) = {}, long style = wxLC_LIST | wxLC_EDIT_LABELS)
		: wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize, style), sourceClass(sourceClass), OnSelect(OnSelect),
		OnRightClick(OnRightClick) { }

	void Fill(std::size_t n_args, ...) {
		va_list args;
		va_start(args, n_args);

		for (std::size_t i = 0; i < n_args; i++)
			InsertItem(i, wxString(va_arg(args, const char*)));

		va_end(args);
	}

	void OnItemSelected(wxListEvent& event) { if (OnSelect != NULL && sourceClass != NULL) (sourceClass->*OnSelect)(event); }
	void OnItemRightClick(wxListEvent& event) { if (OnRightClick != NULL && sourceClass != NULL) (sourceClass->*OnRightClick)(event); }

private:
	wxDECLARE_NO_COPY_CLASS(MathList);
	wxDECLARE_EVENT_TABLE();
};
#endif