#ifndef _SimpleInterpreter
#define _SimpleInterpreter
#include <iostream>
#include <algorithm>
#include <string>
#include "Expresion.h"

using namespace std;

namespace m_Interpreter {

	enum Direction { BACKWARDS, FORWARD };

	Value scanValue(string indexed, size_t position, Direction direction);

	struct ValueConstraint {
		ValueConstraint() : vc_value(make_shared<Value>(1)) {}
		ValueConstraint(Value value) : vc_value(make_shared<Value>(value)) {}
		ValueConstraint(Value value, size_t begin, size_t end) : vc_value(make_shared<Value>(value)), vc_begin(begin), vc_end(end) {}
		shared_ptr<Value> vc_value;
		size_t vc_begin, vc_end;

		inline bool operator==(const ValueConstraint &other) const {
			return other.vc_value == vc_value && other.vc_begin == vc_begin && other.vc_end == vc_end;
		}
	};

	class SimpleInterpreter {
		Expresion si_result;

		MathTerm analizeTerm(string term);

	public:
		SimpleInterpreter(string raw);

		Expresion &getResult() {
			return si_result;
		}
	};

}

#endif
