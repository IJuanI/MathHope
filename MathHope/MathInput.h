#ifndef _MathInput
#define _MathInput
#include "History.h"
#include "wx/html/htmlwin.h"
#include "wx/html/htmlproc.h"
#include "Utils.h"
#include "Expresion.h"
#include "Properties.h"
#include "Interpreter.h"

enum class CalcMode { Simply, Expand, Compare, Other };

namespace m_Colors {
	const wxColour def(120, 120, 140);
	const wxColour success(100, 230, 100);
	const wxColour error(230, 100, 100);
	const wxColour simply(100, 200, 200);
	const wxColour expand(200, 100, 100);
	const wxColour compare(100, 200, 100);
}

Expresion getAsociated(const std::string input);
wxString calculate(const std::string &input, CalcMode mode);
bool compare(const std::string &primary, const std::string &secondary);
std::string readHTML(const std::string input);

enum CustomIDS {
	m_ListCTRL, bottom_panel, settings_panel,
	button_calc, button_clearHistory, button_clearInput,
	simp_input, simp_History,
	exp_input, exp_History,
	comp_input, comp_input2, comp_History, comp_History2, comp_result, comp_panel,
	settings_slider, settings_fontG, settings_fontI, settings_color, settings_sldtxt, settings_fonttxt, settings_fontItxt, settings_clrtxt
};

class MathInput : public wxHtmlWindow {
	CalcMode mode;
	std::string rawContent;
	std::vector<Utils::Entry<std::size_t, std::size_t>> powIndexes;
	std::size_t index = 0;
	std::size_t contentSize = 0;
	wxWindow *parent;
	wxWindowID id;
	friend class History;
public:
	History *output = NULL;
	bool shouldClear = false;
	std::string content;
	std::size_t currentLvl = 0;
	static const std::string header;
	static const std::string footer;
	static const std::string powStart;
	static const std::string powEnd;
	static const std::string boldStart;
	static const std::string boldEnd;

	MathInput(wxWindow *parent, CalcMode mode, wxWindowID id = -1, long style = wxHW_SCROLLBAR_NEVER)
		: wxHtmlWindow(parent, id, wxDefaultPosition, wxDefaultSize, style), mode(mode), parent(parent), id(id) {
		Connect(wxEVT_CHAR, wxKeyEventHandler(MathInput::OnChar), NULL, this);
	}

	void SetOutput(History *history) {
		output = history;
	}

	void Clear() {
		content.clear();
		rawContent.clear();
		powIndexes.clear();
		currentLvl = 0;
		index = 0;
		contentSize = 0;
		SetPage("");
	}

	void OnChar(wxKeyEvent& event) {
		if (shouldClear) {
			parent->GetWindowChild(comp_result)->SetBackgroundColour(m_Colors::def);
			if (id == comp_input)
				dynamic_cast<MathInput*>(parent->GetWindowChild(comp_input2))->shouldClear = false;
			else if (id == comp_input2)
				dynamic_cast<MathInput*>(parent->GetWindowChild(comp_input))->shouldClear = false;
			shouldClear = false;
			parent->Refresh();
		}

		if (event.GetKeyCode() == '/' || event.GetKeyCode() == '.' || event.GetKeyCode() == ',') {

		} else if (event.GetKeyCode() == '^') {
			size_t size = content.size();
			auto iter = find(powIndexes, (Func<Entry<size_t, size_t>>) [&size](Entry<size_t, size_t> entry) { return *entry.en_key + *entry.en_value == size; });
			if (content.size() > 0 && iter == powIndexes.end()) {
				char ch = content.back();
				if (ch != '+' && ch != '-' && ch != '*') {
					currentLvl++;
					powIndexes.emplace_back(content.size(), powStart.size());
					content.insert(content.end(), powStart.begin(), powStart.end());
				}
			}

		}
		else if (event.GetKeyCode() == 314) {
			if (index > 0)
				index--;
		}
		else if (event.GetKeyCode() == 316 || event.GetKeyCode() == 315) {
			if (index < contentSize)
				index++;
		}
		else if (event.GetKeyCode() == 317)
			if (currentLvl > 0) {
				powIndexes.emplace_back(content.size(), powEnd.size());
				content.insert(content.end(), powEnd.begin(), powEnd.end());

				currentLvl--;
			}
			else
				index--;
		else if (event.GetKeyCode() == 8) {
			if (index > 0) {
				std::size_t offset = 0;
				for (auto i : powIndexes)
					if (*i.en_key < index + offset)
						offset += *i.en_value;
					else if (index != 0 && *i.en_key == index + offset + 1) {
						if (content.substr(*i.en_key - 1, *i.en_value) == powEnd) {
							index--;
							content.erase(content.begin() + *i.en_key - 1);
							rawContent.erase(rawContent.begin() + index);
							for (auto &l : powIndexes)
								if (*l.en_key >= *i.en_key)
									(*l.en_key)--;
							contentSize--;
							goto END;
						}
						std::size_t end = *i.en_key + *i.en_value;

						bool found = false;
						std::size_t k = 0;
						for (std::size_t j(end); j < content.size(); j++)
							if (content.at(j) == powEnd.at(k++))
								if (k == powEnd.size()) {
									size_t start = j - powEnd.size();
									content.erase(content.begin() + start, content.begin() + j);
									powIndexes.erase(Utils::find(powIndexes, (Func<Utils::Entry<std::size_t, std::size_t>>)
										[&start](Utils::Entry<std::size_t, std::size_t> entry) { return *entry.en_key = start; }));

									for (auto &l : powIndexes)
										if (*l.en_key >= start)
											(*l.en_key) -= powEnd.size();
									found = true;
									break;
								}

						if (!found)
							currentLvl--;

						powIndexes.erase(std::find(powIndexes.begin(), powIndexes.end(), i));
						content.erase(content.begin() + *i.en_key, content.begin() + end);

						for (auto &l : powIndexes)
							if (*l.en_key >= *i.en_key)
								(*l.en_key) -= powStart.size();

						goto END;
					}
					index--;
					content.erase(content.begin() + offset + index);
					rawContent.erase(rawContent.begin() + index);
					offset += index;
					for (auto &i : powIndexes)
						if (*i.en_key >= offset + 1)
							(*i.en_key)--;
					contentSize--;
			}
		} else if (event.GetKeyCode() == 13) {
			if (output != NULL && !content.empty()) 
				if (mode != CalcMode::Compare) {
					std::string builder(content.begin(), content.end());

					std::size_t lvlCopy = currentLvl;
					while (lvlCopy != 0 && lvlCopy-- >= 0)
						builder.insert(builder.end(), powEnd.begin(), powEnd.end());

					output->Log(calculate(readHTML(builder), mode));
					Clear();
				}
		}
		else {
			char c = event.GetKeyCode();
			if (c == '/' || c == '.' || c == ',')
				return;
			std::size_t offset = 0;

			for (auto i : powIndexes)
				if (*i.en_key <= index + offset)
					offset += *i.en_value;
			content.insert(content.begin() + offset + index, c);
			rawContent.insert(rawContent.begin() + index, c);
			offset += index;
			for (auto &i : powIndexes)
				if (*i.en_key > offset + 1)
					(*i.en_key)++;
			contentSize++;
			index++;
		}
	END:

		std::string heading = header;
		wxString family = GetFont().GetFaceName();
		std::size_t size = GetFont().GetPointSize() / 4;
		heading.append(family).append("\" size=\"").append(std::to_string(static_cast<int>(size))).append("\">");

		std::string builder(heading.begin(), heading.end());

		std::size_t i = 0;
		for (char ch : content) {
			if (ch == rawContent[i]) {
				if (i == index && i != contentSize) {
					builder.insert(builder.end(), boldStart.begin(), boldStart.end());
					builder.push_back(ch);
					builder.insert(builder.end(), boldEnd.begin(), boldEnd.end());
					i++;
					continue;
				}
				i++;
			}
			builder.push_back(ch);
		}

		std::size_t lvlCopy = currentLvl;
		while (lvlCopy != 0 && lvlCopy-- >= 0)
			builder.insert(builder.end(), powEnd.begin(), powEnd.end());
		builder.insert(builder.end(), footer.begin(), footer.end());
		SetPage(wxString(builder));
	}

	void UpdateInput() {
		std::string heading = header;
		wxString family = GetFont().GetFaceName();
		std::size_t size = GetFont().GetPointSize() / 4;
		heading.append(family).append("\" size=\"").append(std::to_string(static_cast<int>(size))).append("\">");

		std::string builder(heading.begin(), heading.end());

		std::size_t i = 0;
		for (char ch : content) {
			if (ch == rawContent[i]) {
				if (i == index && i != contentSize) {
					builder.insert(builder.end(), boldStart.begin(), boldStart.end());
					builder.push_back(ch);
					builder.insert(builder.end(), boldEnd.begin(), boldEnd.end());
					i++;
					continue;
				}
				i++;
			}
			builder.push_back(ch);
		}

		std::size_t lvlCopy = currentLvl;
		while (lvlCopy != 0 && lvlCopy-- >= 0)
			builder.insert(builder.end(), powEnd.begin(), powEnd.end());
		builder.insert(builder.end(), footer.begin(), footer.end());
		SetPage(wxString(builder));
	}

	MathTerm checkComponents(MathTerm source) {
		MathTerm fix(source.t_sign);
		for (Component c : source.getComponents()) {
			if (c.hasExponent() || !c.getValue().isComplex())
				fix << c;
			else {
				Expresion sub = *c.getValue().complexValue;
				if (sub.m_count != 1)
					fix << c;
				else
					for (Component subC : checkComponents(sub[0]).t_components)
						fix << subC;
			}
		}
		return fix;
	}

	void SetFromHistory(const wxString &text) {
		Expresion parsed = Interpreter(text.ToStdString()).getExpresion();
		std::vector<MathTerm> terms;
		
		for (MathTerm term : parsed.getTerms())
			terms.emplace_back(checkComponents(term));

		Expresion fixed;
		for (MathTerm term : terms)
			fixed += term;

		powIndexes.clear();
		content = fixed.toHTMLString(0, powIndexes);

		rawContent = content;
		std::string::size_type offset = 0;
		while ((offset = rawContent.find(MathInput::powStart, offset)) != std::string::npos) {
			rawContent.erase(offset, MathInput::powStart.size());
			offset++;
		}

		offset = 0;
		while ((offset = rawContent.find(MathInput::powEnd, offset)) != std::string::npos) {
			rawContent.erase(offset, MathInput::powEnd.size());
			offset++;
		}

		contentSize = rawContent.size();
		index = contentSize;
		currentLvl = 0;

		UpdateInput();
	}
};
#endif