#include "Utils.h"
#include <cmath>

bool Utils::contains(std::vector<char> indexed, std::vector<char> posibilities) {
	for (auto iter = indexed.begin(); iter != indexed.end(); ++iter)
		for (auto iter2 = posibilities.begin(); iter2 != posibilities.end(); ++iter2)
			if (*iter == *iter2)
				return true;

	return false;
}

bool Utils::contains(std::string indexed, std::vector<char> posibilities) {
	for (auto iter = indexed.begin(); iter != indexed.end(); ++iter)
		for (auto iter2 = posibilities.begin(); iter2 != posibilities.end(); ++iter2)
			if (*iter == *iter2)
				return true;

	return false;
}

bool Utils::brackets(std::vector<char> indexed) {
	return contains(indexed, std::vector<char>{'*', '^', '+', '-'});
}

bool Utils::brackets(std::string indexed) {
	return contains(indexed, std::vector<char>{'*', '^', '+', '-'});
}

std::vector<size_t> primeCache = std::vector<size_t> { 2, 3, 5, 7 };

void generatePrimes(size_t amount) {
	for (size_t i = primeCache.size(); i < amount; i++) {
		size_t n = primeCache[i - 1];

		while (true) {
			n+= 2;
			for (size_t prime : primeCache)
				if (n % prime == 0)
					goto GOON;
			break;
		GOON:
			continue;
		}

		primeCache.push_back(n);
	}
}

std::vector<size_t> &Utils::getCache() {
	return primeCache;
}

std::vector<size_t> Utils::getPrimes(size_t amount) {
	if (primeCache.size() < amount)
		generatePrimes(amount);
	
	std::vector<size_t> result;
	for (size_t i = 0; i < amount; i++)
		result.emplace_back(primeCache[i]);

	return result;
}

std::vector<size_t> Utils::getPrimes(size_t start, size_t amount) {
	if (primeCache.size() < amount)
		generatePrimes(amount);

	std::vector<size_t> result;
	for (size_t i = start; i < amount; i++)
		result.emplace_back(primeCache[i]);

	return result;
}

bool Utils::isPrime(size_t num) {
	if (*(primeCache.end() - 1) > num)
		return find(primeCache, num) != primeCache.end();
	else if (num <= 1000) {
		auto primes = getPrimesWithMax(num);
		return find(primes, num) != primes.end();
	} else {
		for (size_t prime : primeCache)
			if (num % prime == 0)
				return false;
		size_t sqNum = static_cast<size_t>(sqrt(num));
		size_t i = *(primeCache.end()-1);
		while (i <= sqNum) {
			i += 2;
			if (num % i == 0)
				return false;
		}

		return true;
	}
}

std::vector<size_t> Utils::getPrimesWithMax(size_t max) {
	size_t i = primeCache.size();
	bool intensive = max > 1000;

	if (primeCache[i - 1] >= max)
		goto BUILD;

	if (intensive)
		primeCache.resize(static_cast<size_t>(sqrt(max)));
	
	while (true) {
		size_t n = primeCache[i - 1];

		while (true) {
			n+= 2;
			for (size_t j = 0; j < i && (primeCache[j]^2) < n; j++)
				if (n % primeCache[j] == 0)
					goto GOON;
			break;
		GOON:
			continue;
		}

		if (intensive) {
			primeCache[i] = n;
		} else
			primeCache.push_back(n);

		i++;

		if (n >= max)
			break;
	}

	primeCache.resize(i);

BUILD:
	std::vector<size_t> result;
	auto iter = Utils::find(primeCache, (std::function<bool(size_t)>) [max](size_t current) { return current > max; });
	for (auto j = primeCache.begin(); j != iter; j++)
		result.emplace_back(*j);

	return result;
}