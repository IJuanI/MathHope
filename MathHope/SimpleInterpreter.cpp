#include "SimpleInterpreter.h"

namespace m_Interpreter {
	Value scanValue(string indexed, size_t position, Direction direction) {
		size_t move = direction == FORWARD ? 1 : -1;
		size_t begin = position;
		bool digit = isdigit(indexed.at(position += move));

		while (digit && isdigit(indexed.at(position += move))) {}

		if (digit) {
			if (direction == FORWARD)
				begin += 1;
			else
				position += 1;
			return Value(static_cast<size_t>(stof(indexed.substr(begin, position-begin))));
		} else
			return Value(getUnknown(indexed[position]));
	}

	MathTerm SimpleInterpreter::analizeTerm(string term) {
		if (term.size() < 2)
			return MathTerm(term[0] == '+');
		MathTerm result(term.at(0) == '+');

		vector<size_t> powDir;
		vector<vector<size_t>> compDir;

		for (size_t i = 1; i < term.size(); i++) {
			char c = term.at(i);
			if (c == '^') {
				vector<size_t> &dir = compDir.at(compDir.size() - 1);
				if (dir.size() < 2)
					dir.push_back(i);
				powDir.push_back(i);
			} else if (c == '*') {
				if (compDir.size() != 0) {
					vector<size_t> &last = compDir[compDir.size() - 1];
					if (last.size() < 2)
						last.push_back(i);
				}
			} else
				if (!isdigit(term.at(i))) {
					if (compDir.size() != 0) {
						vector<size_t> &last = compDir[compDir.size() - 1];
						if (last.size() < 2)
							last.push_back(i);
					}

					compDir.emplace_back(vector<size_t> { i, i + 1 });
				} else
					if (compDir.size() == 0)
						compDir.emplace_back(vector<size_t> { 1 });
					else
						if (compDir[compDir.size() - 1].size() >= 2)
							compDir.emplace_back(vector<size_t> { i });
		}

		vector<size_t> &last = compDir[compDir.size() - 1];
		if (last.size() < 2)
			last.push_back(term.size());

		vector<ValueConstraint> values;

		for (vector<size_t> indexes : compDir) {
			size_t begin = indexes.at(0), end = indexes.at(1);

			if (end - begin == 1 && !isdigit(term.at(begin)))
				values.emplace_back(ValueConstraint(getUnknown(term.at(begin)), begin, end));
			else if (end - begin > 0)
				values.emplace_back(ValueConstraint(static_cast<size_t>(stof(term.substr(begin, end-begin))), begin, end));
			else
				throw runtime_error("Hmm, wtf?");
		}

		vector<vector<ValueConstraint>> pows;

		//Use ValueConstraint to construct Components. Remember ^
		for (size_t pow : powDir)
			if (pow != 0 && pow != term.size() - 1) {
				auto iterLeft = find(values, (Func<ValueConstraint>) [&pow](ValueConstraint constraint)
				{ return constraint.vc_end == pow; });
				auto iterRight = find(values, (Func<ValueConstraint>) [&pow](ValueConstraint constraint)
				{ return constraint.vc_begin == pow + 1; });
				if (iterLeft != values.end() && iterRight != values.end())
					pows.emplace_back(vector<ValueConstraint>{*iterLeft, *iterRight });
			}

		for (vector<ValueConstraint> pow : pows) {
			ValueConstraint &base = pow[0], exponent = pow[1];
			values.erase(find(values, base));
			values.erase(find(values, exponent));

			result += Component(base.vc_value, exponent.vc_value);
		}

		for (ValueConstraint value : values)
			result += Component(value.vc_value);

		return result;
	}

	SimpleInterpreter::SimpleInterpreter(string raw) {
		vector<size_t> ocurrences;

		switch (raw.at(0)) {
		case '+':
		case '-':
			break;
		default:
			raw.insert(0, "+");
		}

		for (size_t i = 0; i < raw.size(); i++) {
			char c = raw.at(i);

			if (c == '+' || c == '-')
				ocurrences.push_back(i);
		}

		for (size_t i = 0; i < ocurrences.size(); i++) {
			size_t start = ocurrences.at(i);
			size_t end = i == ocurrences.size() - 1 ? raw.size() : ocurrences.at(i + 1);

			si_result += analizeTerm(raw.substr(start, end-start));
		}

	}
}
