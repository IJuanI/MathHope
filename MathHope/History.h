#ifndef _MathHistory
#define _MathHistory
#include "wx/listctrl.h"

class MathInput;

class History : public wxListCtrl {
	MathInput *input;

public:
	History(wxWindow *parent, MathInput *input, wxWindowID id = -1, long style = wxLC_LIST);

	void Log(wxString text) {
		InsertItem(GetItemCount(), text);
	}

	void OnRightClick(wxListEvent &event);
	void OnClose(wxCloseEvent &event);

private:
	wxDECLARE_NO_COPY_CLASS(History);
	wxDECLARE_EVENT_TABLE();
};

#endif