#ifndef _MathSimplifier
#define _MathSimplifier

#include "Expresion.h"
#include "Operations.h"
#include <set>

using namespace Operations;

class Simplifier {
	Expresion s_result;

	Expresion checkChilds(const Expresion &expresion) const;

	std::vector<size_t> getPossibilities(const std::vector<size_t> &list, size_t val = 0);

public:
	Simplifier(const Expresion &expresion);

	Expresion &getResult();
};

#endif
