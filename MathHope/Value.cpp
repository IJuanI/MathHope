#include "Expresion.h"
#include <iostream>
#include <sstream>

Value::Value(const Value& other) : m_isComplex(other.m_isComplex), m_isKnown(other.m_isKnown), m_isConstant(other.m_isConstant) {
	if (other.isComplex())
		complexValue = std::make_unique<Expresion>(*other.complexValue);
	else if (other.isKnown())
		healtyValue = std::make_unique<size_t>(*other.healtyValue);
	else
		v_unknown = std::make_unique<Unknown>(*other.v_unknown);
}

Value::Value(const size_t &value) : healtyValue(std::make_unique<size_t>(value)) {}
Value::Value(const Unknown &value) : v_unknown(std::make_unique<Unknown>(value)) {
	m_isKnown = false;
	m_isConstant = false;
}

Value::Value(const Expresion &value) : complexValue(std::make_unique<Expresion>(value)) {
	m_isComplex = true;
	m_isKnown = false;
	m_isConstant = false;
}

Value &Value::operator=(Value &other) {
	m_isComplex = other.m_isComplex;
	m_isKnown = other.m_isKnown;
	m_isConstant = other.m_isConstant;

	if (other.isComplex())
		complexValue = std::move(other.complexValue);
	else if (other.isKnown())
		healtyValue = std::move(other.healtyValue);
	else
		v_unknown = std::move(other.v_unknown);
	return *this;
}

bool Value::operator==(const Value &other) const {
	return (m_isKnown && other.m_isKnown && *healtyValue == *other.healtyValue)
		|| (m_isComplex && other.m_isComplex && *complexValue == *other.complexValue)
		|| (!(m_isKnown || other.m_isKnown || m_isComplex || other.m_isComplex) && *v_unknown == *other.v_unknown);
}

bool Value::operator==(const Unknown &unknown) const {
	return !m_isComplex && !m_isKnown && *this->v_unknown == unknown;
}

bool Value::operator!=(const Value &other) const {
	return !operator==(other);
}

std::shared_ptr<Value> Value::operator^(const Value &value) const {
	if (m_isKnown && value.m_isKnown) {
		size_t aux = static_cast<size_t>(std::pow(*healtyValue, *value.healtyValue));
		return std::make_shared<Value>(aux);
	} else
		return std::make_shared<Value>(Expresions::getExpresion(Component(*this, Value(value))));
}

std::shared_ptr<Value> Value::operator*(const Value &value) const {
	if (value.m_isKnown) 
		if (m_isKnown) {
			size_t val = *healtyValue * *value.healtyValue;
			return std::make_shared<Value>(val);
		} else if (m_isComplex)
			return std::make_shared<Value>(*complexValue * *value.healtyValue);
		else
			return std::make_shared<Value>(*v_unknown * *value.healtyValue);
	else if (m_isKnown)
		if (value.m_isComplex)
			return std::make_shared<Value>(*value.complexValue * *healtyValue);
		else
			return std::make_shared<Value>(*value.v_unknown * *healtyValue);
	else if (m_isComplex && value.m_isComplex) {
		Expresion expresion = Expresion(*complexValue), expresion2 = *value.complexValue;
		for (MathTerm term : expresion.m_terms)
			term *= expresion2;
		return std::make_shared<Value>(expresion);
	} else if (m_isComplex || value.m_isComplex) {
		Expresion expresion;
		Unknown unknown;

		if (m_isComplex) {
			expresion = Expresion(*complexValue);
			unknown = *value.v_unknown;
		} else {
			expresion = Expresion(*value.complexValue);
			unknown = *this->v_unknown;
		}

		for (MathTerm term : expresion.m_terms)
			term *= unknown;

		return std::make_shared<Value>(expresion);
	} else
		return std::make_shared<Value>(Expresions::getExpresion(Component(*this, Value::TWO())));
}

std::shared_ptr<Value> Value::operator*(size_t value) const {
	if (m_isKnown)
		return std::make_shared<Value>(*healtyValue * value);
	else if (m_isComplex)
		return std::make_shared<Value>(*complexValue * value);
	else
		return std::make_shared<Value>(*v_unknown * value);
}

std::shared_ptr<Value> Value::operator+(const Value &value) const {
	if (operator==(value))
		return operator*(2);
	else if (m_isKnown && value.m_isKnown) {
		size_t temp = *healtyValue + *value.healtyValue;
		return std::make_shared<Value>(temp);
	} else if (!m_isComplex && !value.m_isComplex)
		return Values::directSum(*this, value);
	else if (!m_isComplex)
		return std::make_shared<Value>(*value.complexValue + *this);
	else
		return std::make_shared<Value>(*complexValue + value);	
}

std::shared_ptr<Value> Value::operator+(size_t value) const {
	if (m_isKnown)
		return std::make_shared<Value>(*healtyValue + value);
	else
		return operator+(Value(value));
}

std::vector<char> Value::toString() const {
	std::vector<char> result;
	if (m_isKnown) {
		std::ostringstream ss;
		ss << *healtyValue;
		for (char c : ss.str())
			result.push_back(c);
	}
	else if (m_isComplex)
		return complexValue->toString();
	else
		result.push_back(v_unknown->m_key);

	return result;
}

std::string Value::toHTMLString(std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const {
	std::string result;
	if (m_isKnown) {
		std::ostringstream ss;
		ss << *healtyValue;
		result = ss.str();
	}
	else if (m_isComplex)
		return complexValue->toHTMLString(overall, powIndexes);
	else
		result.push_back(v_unknown->m_key);

	return result;
}