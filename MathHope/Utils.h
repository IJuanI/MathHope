#ifndef _MathMyUtils
#define _MathMyUtils

#include <vector>
#include <functional>
#include <memory>

template<typename T>
using Func = std::function<bool(T)>;

namespace Utils {
	
	enum CompareMode { LEFT, RIGHT, BOTH };
	enum Storage { Parent, Childs, Root };
	
	template <typename K, typename V>
	struct Entry {
	private:
		CompareMode en_mode = BOTH;
	public:
		std::shared_ptr<K> en_key;
		std::shared_ptr<V> en_value;

		Entry() : en_key(std::make_shared<K>()), en_value(std::make_shared<V>()) {}
		Entry(const K &key, const V &value) : en_key(std::make_shared<K>(key)), en_value(std::make_shared<V>(value)) { }

		void setCompareMode(CompareMode mode) {
			this->en_mode = mode;
		}


		bool operator==(const Entry &other) const {
			switch (en_mode) {
			case LEFT:
				return en_key == other.en_key;
			case RIGHT:
				return en_value == other.en_value;
			default:
				return en_key == other.en_key && en_value == other.en_value;
			}
		}
		
		Entry<K, V> &operator=(const Entry<K, V> &other) {
			en_key = std::make_shared<K>(*other.en_key);
			en_value = std::make_shared<V>(*other.en_value);
			return *this;
		}
	};

	template<typename K, typename V>
	typename std::vector< Entry<K, V>>::iterator  findKey(typename std::vector< Entry<K, V>> &compared, K &key) {
		for (auto iter = compared.begin(); iter != compared.end(); ++iter)
			if (iter->key == key)
				return key;

		return compared.end();
	}

	template<typename K, typename V>
	typename std::vector< Entry<K, V>>::iterator  findValue(typename std::vector< Entry<K, V>> &compared, V &value) {
		for (auto iter = compared.begin(); iter != compared.end(); ++iter)
			if (iter->value == value)
				return iter;

		return compared.end();
	}

	template<typename T>
	typename std::vector<T>::iterator find(typename std::vector<T> &compared, T &value) {
		for (auto iter = compared.begin(); iter != compared.end(); ++iter)
			if (*iter == value)
				return iter;

		return compared.end();
	}

	template<typename T>
	typename std::vector<T>::iterator find(typename std::vector<T> &compared, const std::function<bool(T)> &compare) {
		for (auto iter = compared.begin(); iter != compared.end(); ++iter)
			if (compare(*iter))
				return iter;

		return compared.end();
	}

	template<typename T>
	typename std::vector<T>::const_iterator findConst(const typename std::vector<T> &compared, const std::function<bool(T)> &compare) {
		for (auto iter = compared.cbegin(); iter != compared.cend(); ++iter)
			if (compare(*iter))
				return iter;

		return compared.cend();
	}

	template<typename T>
	typename std::vector<T>::const_iterator findConst(const typename std::vector<T> &compared, const T &value) {
		for (auto iter = compared.cbegin(); iter != compared.cend(); ++iter)
			if (*iter == value)
				return iter;

		return compared.cend();
	}

	bool contains(std::string indexed, std::vector<char> posibilities);
	bool contains(std::vector<char> indexed, std::vector<char> posibilities);
	bool brackets(std::string indexed);
	bool brackets(std::vector<char> indexed);

	std::vector<size_t> &getCache();
	std::vector<size_t> getPrimes(size_t amount);
	std::vector<size_t> getPrimes(size_t start, size_t amount);
	bool isPrime(size_t n);
	std::vector<size_t> getPrimesWithMax(size_t max);
}

#endif
