#include "Operations.h"

using namespace std;

namespace Operations {

	MathTerm simplify(const Component &comp) {
		MathTerm result;
		if (!comp.hasExponent() && comp.getValue().isKnown()) {
			std::vector<Entry<size_t, size_t>> pows;
			ptrdiff_t val = *comp.getValue().healtyValue;

			if (val < 1000)
				if (val != 1)
					for (size_t prime : getPrimesWithMax(val))
						while (val % prime == 0) {
							auto iter = find(pows, (Func<Entry<size_t, size_t>>) [&prime](Entry<size_t, size_t> entry) { return *entry.en_key == prime; });
							if (iter != pows.end())
								(*iter->en_value)++;
							else
								pows.emplace_back(prime, 1);
							val = val / prime;
						}
				else
					pows.emplace_back(1, 1);
			else {
				auto &cache = getCache();
				size_t i = 0;
				size_t sqval = static_cast<size_t>(sqrt(val));
				while (true) {
					size_t prime;

					if (i < cache.size())
						prime = cache[i];
					else {
						size_t n = cache[i - 1];
						while (true) {
							n++;
							for (size_t prime : cache)
								if (n % prime == 0)
									goto GOON;
							break;
						GOON:
							continue;
						}
						prime = n;
						cache.push_back(n);
					}

					while (val % prime == 0) {
						auto iter = find(pows, (Func<Entry<size_t, size_t>>) [&prime](Entry<size_t, size_t> entry) { return *entry.en_key == prime; });
						if (iter != pows.end())
							(*iter->en_value)++;
						else
							pows.emplace_back(Entry<size_t, size_t>(prime, 1));
						val = val / prime;
						sqval = static_cast<size_t>(sqrt(val));
					}

					if (prime > sqval && prime != val)
						break;

					i++;
				}

				if (val != 1)
					pows.emplace_back(Entry<size_t, size_t>(val, 1));
			}

			for (Entry<size_t, size_t> entry : pows)
				result << Component(*entry.en_key, *entry.en_value);
		}
		else
			result << Component(comp);
		return result;
	}

	Expresion getCommon(const Expresion &source) {
		Expresion result;
		std::vector<Component> factors;
		std::vector<MathTerm> terms = source.getTerms();
		bool aux = true;

		for (MathTerm &term : terms)
			for (Component &comp : term.getComponents())
				if (comp.isCompactable()) {
					term >> comp;
					for (Component c : simplify(Component(comp.getCompact())).t_components)
						term << c;
				}

		for (MathTerm &term : terms) {
			if (aux)
				for (Component &comp : term.getComponents())
					factors.emplace_back(comp);
			else {
				std::vector<size_t> queue;
				for (auto iter = factors.begin(); iter != factors.end(); ++iter) {
					const Value &val = iter->getValue();
					auto &i = find(term.t_components,
						(Func<Component>) [&val](Component comp) { return comp.getValue() == val; });

					if (i == term.t_components.end())
						queue.emplace_back(iter - factors.begin());
					else if (i->getExponent() != iter->getExponent())
						if (i->getExponent().isComplex())
							queue.emplace_back(iter - factors.begin());
						else if (*i->getExponent().healtyValue < *iter->getExponent().healtyValue)
							iter->sumToExponent(*i->getExponent().healtyValue - *iter->getExponent().healtyValue);
				}

				std::vector<size_t> erased;

				for (size_t current : queue) {
					size_t offset = 0;
					for (size_t old : erased)
						if (old < current)
							offset++;
						else if (old == current)
							goto END;

					factors.erase(factors.begin() + (current - offset));
					erased.emplace_back(current);
				END:
					continue;
				}
			}

			aux = false;
		}

		MathTerm base;

		for (Component &factor : factors) {
			for (MathTerm &term : terms)
				for (Component &comp : term.getComponents())
					/* If you find a comp with same value as factor
					If both are equal, remove them from term
					Else if exponents are known, subtract them (comp - factor) */
					if (comp.getValue() == factor.getValue())
						if (comp == factor) {
							term >> comp;
							if (term.isEmpty())
								term << Value::ONE();
						} else if (comp.getExponent().isKnown() && factor.getExponent().isKnown()) {
							term >> comp;
							comp.sumToExponent(-*factor.getExponent().healtyValue);
							term << comp;
						}

						base += factor;
		}
		/* Then make everything be multiplied by factors here

		Ohh, and remember bout known values (Won't get factorized) */

		if (base.isEmpty())
			for (MathTerm &term : terms)
				result += term;
		else {
			Expresion sub;
			for (MathTerm &term : terms)
				sub += term;
			if (sub.m_count == 1 && sub[0].t_count == 1 && sub[0][0].isCompactable()) {
				Value &compact = sub[0][0].getCompact();
				if (compact != Value::ONE())
					base << compact;
			} else
				base << Component(sub);
			result += base;
		}

		return result;
	}

	Expresion getCommonByGroup(const Expresion &source, size_t div) {
		Expresion result;

		if (div != 1 && div != source.m_count) {
			vector<Component> indexes;
			vector<vector<Entry<size_t, Value>>> termsInfo; //compKey, exponent
			vector<MathTerm> &terms = source.getTerms();

			size_t i = 0;
			for (MathTerm &term : terms) {
				vector<Entry<size_t, Value>> info;
				for (Component &comp : term.t_components) {
					auto iter = find(indexes, comp);
					size_t compIndex;
					if (iter == indexes.end()) {
						compIndex = indexes.size();
						indexes.emplace_back(comp);
					} else
						compIndex = distance(indexes.begin(), iter);
					info.emplace_back(compIndex, comp.getExponent());
				}
				termsInfo.emplace_back(info);
				i++;
			}

			vector<Entry<size_t, size_t>> blacklist;
			vector<vector<Entry<size_t, size_t>>> groups;

			size_t currentGroup = 0;
			size_t groupIndex = 0;
			size_t j = 0;
			size_t k = 0;

			for (; j < indexes.size(); j++) {
				Component &jVal = indexes[j];
				for (k = 0; k < termsInfo.size(); k++) {
				GroupCreation:
					if (find(blacklist, (Func<Entry<size_t, size_t>>) [&k](Entry<size_t, size_t> i)
					{ return *i.en_value == k; }) != blacklist.end())
						continue;

					vector<Entry<size_t, Value>> &info = termsInfo[k];
					if (find(info, (Func<Entry<size_t, Value>>) [&j, &jVal](Entry<size_t, Value> entry) { return *entry.en_key == j
						|| (entry.en_value->isKnown() && jVal.getExponent().isKnown()
						&& *entry.en_value->healtyValue > *jVal.getExponent().healtyValue); }) != info.end()) {
						if (currentGroup >= groups.size())
							groups.emplace_back();

						groups[currentGroup].emplace_back(j, k);
						groupIndex++;

						if (groupIndex >= div) {
							groupIndex = 0;
							currentGroup++;
						}
						blacklist.emplace_back(j, k);
					}
				}

				if (groupIndex != 0) {
					size_t n = termsInfo.size() - 1;
					while (n >= 0 && groupIndex > 0) {
						auto iter = find(groups[currentGroup], (Func<Entry<size_t, size_t>>)
							[&n](Entry<size_t, size_t> entry) {return *entry.en_value == n; });
						if (iter != groups[currentGroup].end()) {
							groups[currentGroup].erase(iter);
							blacklist.erase(find(blacklist, (Func<Entry<size_t, size_t>>)
								[&n](Entry<size_t, size_t> entry) { return *entry.en_value == n; }));
							groupIndex--;
						}
						n--;
					}

					if (groupIndex > 0)
						return Expresion();
					else
						groups.erase(groups.begin() + currentGroup);
				}
			}

			size_t eraseCount = 1;

			if (j <= indexes.size() && blacklist.size() > 0 && ((groups.end() - 1)->size() != div || groups.size() * div != source.m_count)) {
			RETRY:
				k = (*(blacklist.end() - 1)->en_value) + 1;
				j = *(blacklist.end() - 1)->en_key;
				if (k >= termsInfo.size()) {
					j++;
					k = 0;
				}

				blacklist.erase(blacklist.end() - 1);

				for (size_t o = 0; o < eraseCount; o++) {
					if (groups.size() <= 0)
						return Expresion();
					auto childIter = groups.end() - 1;
					if (childIter->size() > 0)
						childIter->erase(childIter->end() - 1);
					else
						groups.erase(childIter);
				}


				if (currentGroup < eraseCount / div)
					return Expresion();
				currentGroup -= eraseCount / div;

				if (groupIndex < eraseCount % div)
					if (currentGroup < 1)
						return Expresion();
					else {
						currentGroup--;
						groupIndex -= (eraseCount % div) - div;
					}
				else
					groupIndex -= eraseCount % div;

				goto GroupCreation;
			}


			//Processor, forgive me please

			/*

			1,3 + 2,4 + 1,4 + 2,3
			(3+4)(1+2) = 1(3 + 4) +2(4 + 3) = (1,3 + 1,4) + (2,4 + 2,3)
			(1+2)(3+4) = 3(1 + 2) +4(2 + 1) = (1,3 + 2,3) + (2,4 + 1,4)

			**/

			if (groups.size() > 0 && (groups.end() - 1)->size() == div && groups.size() * div == source.m_count)
				for (vector<Entry<size_t, size_t>> group : groups) {
					Expresion sub;
					for (Entry<size_t, size_t> termIndex : group)
						sub += terms[*termIndex.en_value];

					result += getCommon(sub);
				}

			if (getCommon(result) == result) {
				result = Expresion();
				if (blacklist.size() > 0 && j <= indexes.size()) {
					eraseCount = 1;
					goto RETRY;
				}
			}
		}

		return result;
	}
}