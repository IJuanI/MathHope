#include "BracketContainer.h"

namespace m_Interpreter {

	BracketContainer interpreter_error::m_getContainer() {
		return *ie_errored;
	}

	const BracketContainer &interpreter_error::getContainer() {
		return *ie_errored;
	}
}