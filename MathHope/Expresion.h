#ifndef _MathExpresion
#define _MathExpresion

#include "MathTerm.h"

class Simplifier;

namespace m_Interpreter {
	class ExpresionContainer;
}

class Expresion {
	friend class MathTerm;
	friend class Component;
	friend struct Value;
	friend class Simplifier;
	friend class m_Interpreter::ExpresionContainer;

private:
	std::vector<MathTerm> m_terms;

public:
	size_t m_count = 0;

	Expresion() {}
	Expresion(const MathTerm &term);

	Expresion &operator +=(const Expresion &expresion);
	Expresion &operator +=(const MathTerm &term);
	Expresion &operator -=(const MathTerm &term);
	MathTerm &operator[](size_t index);
	bool operator==(const Expresion &expresion) const;
	bool operator!=(const Expresion &expresion) const {
		return !operator==(expresion);
	}
	Expresion &operator*=(Component &comp);
	Expresion operator*(const Expresion &expresion) const;
	Expresion operator*(size_t value) const;
	Expresion operator+(const Value &value) const;

	std::vector<MathTerm> getTerms() const;
	size_t size();

	bool isMember(const MathTerm &term) const;
	ptrdiff_t index(const MathTerm &term) const;
	ptrdiff_t index(const Value &value) const;

	void clear();
	std::vector<char> toString() const;
	std::string toHTMLString(std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const;
};

#endif