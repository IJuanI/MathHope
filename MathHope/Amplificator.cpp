#include "Amplificator.h"

Expresion Amplificator::amplificate(MathTerm &term) {
	Expresion result;
	std::vector<Component> normals;
	bool isComplex = false;

	for (Component comp : term.getComponents())
		if (comp.getValue().isComplex() && (!comp.hasExponent() || comp.getExponent().isKnown())) {
			isComplex |= true;
			size_t exp = *comp.getExponent().healtyValue;
			for (size_t i = 0; i < exp; i++) {
				if (result.size() < 1)
					result = MathTerm(Value::ONE(), term.t_sign);

				result = result * *comp.getValue().complexValue;
			}
		} else
			normals.emplace_back(comp);

	if (isComplex) {
		if (result.size() < 1)
			result += MathTerm(term.t_sign) << Value::ONE();

		for (Component comp : normals)
			result *= comp;

		return result;
	} else
		return Expresion(term);
}

Amplificator::Amplificator(const Expresion &expresion) {
	for (MathTerm &term : expresion.getTerms())
		a_result += amplificate(term);
}

Expresion &Amplificator::getResult() {
	return a_result;
}