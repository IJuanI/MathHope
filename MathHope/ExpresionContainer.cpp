#include "ExpresionContainer.h"

namespace m_Interpreter {

	void ExpresionContainer::scan(vector<size_t> &path) {
		BracketContainer* actual = e_broRoot;

		for (size_t dir : path)
			actual = actual->b_childs[dir];

		auto broChilds = actual->b_childs;
		for (size_t i = 0; i < broChilds.size(); i++) {
			vector<size_t> pathClone = vector<size_t>(path);
			pathClone.emplace_back(i);

			ExpresionContainer *child = new ExpresionContainer();
			child->e_parent = this;
			child->e_broRoot = e_broRoot;
			child->e_level = e_level + 1;
			child->e_dir = pathClone;

			e_childs.emplace_back(child);

			child->scan(pathClone);
		}
	}

	void ExpresionContainer::processLevel(size_t *current) {
		if (e_level > *current)
			*current = e_level;

		for (ExpresionContainer* child : e_childs)
			child->processLevel(current);
	}

	void ExpresionContainer::getChildsAtLevel(const size_t &level, vector<vector<size_t>> &result) {
		if (e_level == level)
			result.emplace_back(e_dir);

		for (ExpresionContainer* child : e_childs)
			child->getChildsAtLevel(level, result);
	}

	BracketContainer* ExpresionContainer::getBrother(const vector<size_t> &dir) {
		BracketContainer* actual = e_broRoot;
		for (size_t index : dir)
			actual = actual->b_childs[index];
		return actual;
	}

	void ExpresionContainer::m_analize(string raw) {
		Separator separator = getBrother(e_dir)->b_content;
		string toAnalize;

		if (e_level == 0)
			toAnalize = raw.substr(separator.s_begin, separator.s_end - separator.s_begin);
		else
			toAnalize = raw.substr(separator.s_begin + 1, separator.s_end - separator.s_begin - 1);
		if (e_childs.size() != 0) {
			vector<Separator> toIgnore;
			vector<size_t> omissions;

			for (ExpresionContainer* child : e_childs)
				toIgnore.emplace_back(getBrother(child->e_dir)->b_content);

			vector<Separator> changes;
			vector<size_t> powChanges;
			string ignoreString = toAnalize, powFix = toAnalize;
			size_t childOffset = separator.s_begin == 0 ? 0 : separator.s_begin + 1;
			size_t powFixChangesCount = 0;
			for (Separator sep : toIgnore) {
				size_t offset = 0, powOffset = 0;

				find(changes, (Func<Separator>) [sep, &offset](Separator indexed) {
					if (indexed.s_end < sep.s_begin)
						offset += indexed.s_end - indexed.s_begin + 1;
					return false; });

				size_t omited = sep.s_begin - offset;

				for (size_t &change : powChanges)
					if (change < omited)
						powOffset++;

				omissions.emplace_back(omited - childOffset);
				ignoreString.erase(omited - childOffset, sep.s_end - offset - omited + 1);
				powFix.erase(omited - powOffset - childOffset, sep.s_end - offset - omited + 1 + powFixChangesCount);
				if (omited - powOffset - childOffset == 0) {
					powFix.insert(omited - powOffset - childOffset, "*");
					powFixChangesCount++;
				}

				if (!powFix.empty() && powFix.size() > omited - powOffset && powFix.at(omited-powOffset) == '^') {
					powFix[omited - powOffset] = '*';

					if (powFix.size() > omited - powOffset + 1 && powFix[omited - powOffset + 1] != '(') {
						size_t compSize = 1;
						if (isdigit(powFix[omited - powOffset + 1]))
							while (powFix.size() > omited - powOffset + 1 + compSize && isdigit(omited - powOffset + 1 + compSize))
								compSize++;
						powFix.erase(omited - powOffset + 1, compSize);
						for (size_t current = 0; current < compSize; current++)
							powChanges.push_back(omited + current + 1);
					}
				}

				if (!powFix.empty() && omited - powOffset > 1
					&& powFix.size() >= omited - powOffset && powFix.at(omited - 1 - powOffset) == '^') {
					powFix[omited - 1 - powOffset] = '*';

					if (omited - powOffset > 1) {
						size_t backSize = 1;

						if (isdigit(powFix[omited - powOffset - 2]) && omited - powOffset > 2)
							while (omited - powOffset > backSize + 1 && isdigit(powFix[omited - 2 - powOffset - backSize]))
								backSize++;

						powFix.erase(omited - 1 - powOffset - backSize, backSize);
						for (size_t current = 0; current < backSize; current++)
							powChanges.push_back(omited - 2 - powOffset - current);
					}
				}

				changes.emplace_back(sep);
			}

			if (!powFix.empty() && powFix[0] != '+' && powFix[0] != '-')
				powFix.insert(powFix.begin(), '+');

			std::size_t findOffset = 0;
			while ((findOffset = powFix.find('*', findOffset)) != std::string::npos)
				if (findOffset == 0 || findOffset == powFix.size() - 1 || powFix.at(findOffset - 1) == '+'
					|| powFix.at(findOffset - 1) == '-' || powFix.at(findOffset - 1) == '*'
					|| powFix.at(findOffset + 1) == '+' || powFix.at(findOffset + 1) == '-' || powFix.at(findOffset + 1) == '*')
					powFix.erase(powFix.begin() + findOffset);

			vector<size_t> signs;

			if (!powFix.empty()) {
				e_content = SimpleInterpreter(powFix).getResult();

				bool ignoreStart = ignoreString.at(0) == '+' || ignoreString.at(0) == '-';
				for (size_t i = (ignoreStart ? 1 : 0); i < ignoreString.size(); i++) {
					char c = ignoreString.at(i);
					if (c == '+' || c == '-')
						signs.push_back(i);
				}
			} else {
				e_content.m_terms.resize(e_content.m_count + 1);
				e_content.m_count++;

				signs.emplace_back(0);
			}

			vector<size_t> alreadyUsed;
			for (size_t i = 0; i < e_childs.size(); i++) {
				if (find(alreadyUsed, i) != alreadyUsed.end())
					continue;
				size_t compPos = omissions.at(i);
				size_t termPos = distance(signs.begin(), find(signs, (Func<size_t>) [&compPos](size_t sign) { return sign >= compPos; }));

				MathTerm &toModify = e_content[termPos];
				Component toAppend = e_childs[i]->e_content;

				bool top = !ignoreString.empty() && ignoreString.size() > compPos && ignoreString.at(compPos) == '^';
				bool back = !ignoreString.empty() && compPos != 0 && ignoreString.at(compPos - 1) == '^';

				if (top) {
					auto omissionsIter = findConst(omissions, compPos + 1);
					if (omissionsIter == omissions.cend()) {
						if (ignoreString.size() > compPos + 1) {
							char next = ignoreString[compPos + 1];
							if (next >= 48 && next <= 122)
								if (toAppend.hasExponent())
									toAppend = Component(Expresion(toAppend), scanValue(ignoreString, compPos, FORWARD));
								else
									toAppend.m_exponent = std::make_shared<Value>(scanValue(ignoreString, compPos, FORWARD));
						}
					} else {
						size_t powIndex = std::distance(omissions.cbegin(), omissionsIter);
						alreadyUsed.push_back(powIndex);
						if (toAppend.hasExponent())
							toAppend = Component(Expresion(toAppend), e_childs[powIndex]->e_content);
						else if (e_childs[powIndex]->e_content.m_count != 0)
							if (e_childs[powIndex]->e_content.m_count == 1 && e_childs[powIndex]->e_content[0].t_count == 1 && !e_childs[powIndex]->e_content[0][0].hasExponent())
								toAppend.m_exponent = e_childs[powIndex]->e_content[0][0].m_val;
							else
								toAppend.m_exponent = std::make_shared<Value>(e_childs[powIndex]->e_content);
					}
				}

				if (back) {
					if (compPos < 2)
						if (toAppend.hasExponent())
							toAppend = Component(Value::ONE(), Expresion(toAppend));
						else {
							toAppend.m_exponent = toAppend.m_val;
							toAppend.m_val = std::make_shared<Value>(1);
						}
					else {
						char next = ignoreString[compPos - 2];
						if (next >= 48 && next <= 122)
							if (toAppend.hasExponent())
								toAppend = Component(scanValue(ignoreString, compPos, BACKWARDS), Expresion(toAppend));
							else {
								toAppend.m_exponent = toAppend.m_val;
								toAppend.m_val = std::make_shared<Value>(scanValue(ignoreString, compPos - 1, BACKWARDS));
							}
						else
							if (toAppend.hasExponent())
								toAppend = Component(Value::ONE(), Expresion(toAppend));
							else {
								toAppend.m_exponent = toAppend.m_val;
								toAppend.m_val = std::make_shared<Value>(1);
							}
					}
				}

				toModify << toAppend;
			}
		} else
			e_content = SimpleInterpreter(toAnalize).getResult();
	}

	void ExpresionContainer::finalize() {
		for (ExpresionContainer* child : e_childs)
			delete child;
	}

	ExpresionContainer::ExpresionContainer(BracketContainer *brother) : e_broRoot(brother) {
		vector<size_t> path;
		scan(path);
	}

	bool ExpresionContainer::operator==(const ExpresionContainer &other) const {
		bool equal = other.e_content == e_content && other.e_childs.size() == e_childs.size() && other.e_dir == e_dir && other.e_level == e_level;

		if (equal)
			for (size_t i = 0; i < e_childs.size(); i++)
				if (e_childs.at(0) != other.e_childs.at(0))
					return false;

		return equal;
	}

	bool ExpresionContainer::operator!=(const ExpresionContainer &other) const {
		return !operator==(other);
	}

	ExpresionContainer* ExpresionContainer::getChild(size_t index) {
		if (e_childs.size() <= index) {
			size_t oldSize = e_childs.size();
			e_childs.resize(index);

			for (; oldSize < e_childs.size(); oldSize++) {
				ExpresionContainer *child = e_childs[oldSize];
				child->e_parent = this;
				child->e_broRoot = e_broRoot;
				child->e_level = e_level + 1;
				vector<size_t> dir = vector<size_t>(e_dir);
				dir.emplace_back(index);
				child->e_dir = dir;
			}
		}

		return e_childs[index];
	}

	ExpresionContainer* ExpresionContainer::getChild(const vector<size_t> &path) {
		ExpresionContainer *actual = this;
		for (size_t index : path)
			actual = actual->getChild(index);

		return actual;
	}

	ExpresionContainer &ExpresionContainer::analize(string raw) {
		size_t currentLevel = 0;
		processLevel(&currentLevel);

		vector<vector<size_t>> results;
		while (currentLevel != 0) {
			getChildsAtLevel(currentLevel, results);

			for (vector<size_t> dir : results)
				getChild(dir)->m_analize(raw);

			results.clear();
			currentLevel--;
		}

		m_analize(raw);

		return *this;

	}

	ExpresionContainer::~ExpresionContainer() {
		finalize();
	}
}
