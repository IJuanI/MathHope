#ifndef _MathProperties
#define _MathProperties
#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <sstream>
#include "Utils.h"

class Property {
	std::string content;
	bool empty = false;
	Property() { empty = true; }
public:
	static const Property &NONE() { static Property none; return none; }
	Property(std::string content) : content(content) { }
	Property(std::size_t content) : content(std::to_string(content)) { }
	Property(std::vector<std::string> content) {
		bool first = true;
		for (std::string index : content) {
			if (!first)
				this->content.append(", ");
			else
				first = false;
			this->content.append(index);
		}
	}

	Property(std::vector<std::size_t> content) {
		bool first = true;
		for (std::size_t index : content) {
			if (!first)
				this->content.append(", ");
			else
				first = false;
			this->content.append(std::to_string(index));
		}
	}

	bool isEmpty() {
		return empty;
	}

	template<typename T>
	T get() {
		return T();
	}

	template<>
	std::string get<std::string>() { return content; }

	template<>
	std::size_t get<std::size_t>() {
		std::stringstream sstream(content);
		size_t result;
		sstream >> result;
		return result;
	}

	template<>
	std::vector<std::string> get<std::vector<std::string>>() {
		std::vector<std::string> result;

		if (!content.empty()) {
			std::string::size_type pos = 0;
			std::string::size_type last = 0;
			while ((pos = content.find(", ", pos)) != std::string::npos) {
				result.emplace_back(content.substr(last, pos - last));
				last = pos + 2;
				pos++;
			}
			result.emplace_back(content.substr(last, pos - last));
		}

		return result;
	}

	template<>
	std::vector<std::size_t> get<std::vector<std::size_t>>() {
		std::vector<std::size_t> result;

		if (!content.empty()) {
			std::string::size_type pos = 0;
			std::string::size_type last = 0;
			while ((pos = content.find(", ", pos)) != std::string::npos) {
				std::stringstream sstream(content.substr(last, pos - last));
				size_t parsed;
				sstream >> parsed;
				result.push_back(parsed);
				last = pos + 2;
				pos++;
			}

			std::stringstream sstream(content.substr(last, pos - last));
			size_t parsed;
			sstream >> parsed;
			result.push_back(parsed);
		}

		return result;
	}
};

class Properties {
	static std::vector<Utils::Entry<std::string, Property>> properties;
	std::string fileName;
public:
	Properties(const std::string fileName) : fileName("./" + fileName) { }

	void Load() {
		std::string current;
		std::ifstream input(fileName);
		if (input.is_open()) {
			while (std::getline(input, current)) {
				std::string::size_type splitter = current.find_first_of(": ");
				if (splitter != std::string::npos && current.size() > splitter + 2) {
					std::string key = current.substr(0, splitter);
					Property prop = Property(current.substr(splitter + 2));
					properties.emplace_back(key, prop);
				}
			}

			input.close();
		}
	}

	void Save() {
		std::ofstream out;
		out.open(fileName);
		for (auto entry : properties)
			out << *entry.en_key << ": " << entry.en_value->get<std::string>() << "\n";
		out.close();
	}

	static Property getProperty(std::string key);
	static void setProperty(std::string key, Property property);
};

Property getProperty(std::string key);
void setProperty(std::string key, Property property);

#endif