#ifndef _MathComponent
#define _MathComponent

#include "Value.h"

class Asociator;
class Simplifier;

namespace m_Interpreter {
	class ExpresionContainer;
}

namespace Operations {
	MathTerm simplify(const Component &compact);
}

class Component {
private:
	std::shared_ptr<Value> m_exponent, m_val;
	friend class Asociator;
	friend class Simplifier;
	friend class m_Interpreter::ExpresionContainer;
	friend MathTerm Operations::simplify(const Component &compact);

public:
	Component(std::shared_ptr<Value> &val, std::shared_ptr<Value> &exponent);
	Component(const Value &val, const Value &exponent);
	Component(std::shared_ptr<Value> &val);
	Component(const Value &val);
	Component();

	Value &getValue() const;
	Value &getExponent() const;
	Value getCompact() const;
	bool hasExponent() const;
	bool isCompactable() const;
	
	bool operator==(const Component &component) const;
	void sumToExponent(size_t exponent);
	void sumToExponent(const Value &value);
	void operator^=(size_t exponent);
	Component &operator*=(size_t value);
	Component &operator*=(const Component &component);
	bool operator+=(size_t value);
	bool operator+=(const Value &value);

	std::vector<char> toString() const;
	std::string toHTMLString(std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const;
};

#endif