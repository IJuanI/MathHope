#ifndef _MathTerm
#define _MathTerm

#include "Component.h"

class MathTerm {
private:
	bool m_isKnown = true;
	bool m_isCompact = false;
	bool m_isEmpty = true;
	friend class Asociator;

public:
	std::vector<Component> t_components;

	bool t_sign = true;
	size_t t_count = 0;

	MathTerm();
	MathTerm(Value &value);
	MathTerm(const bool &sign);
	MathTerm(const Component &component);
	MathTerm::MathTerm(const Component &value, const bool &sign);

	MathTerm &operator +=(const Component &comp);
	MathTerm &operator <<(const Component &comp);
	MathTerm &operator -=(const Component &comp);
	MathTerm &operator >>(const Component &comp);
	Component &operator[](size_t index);
	MathTerm operator*(size_t value) const;
	MathTerm operator*(const MathTerm &term) const;
	MathTerm &operator*=(size_t value);
	MathTerm &operator*=(const Unknown &unknown);
	MathTerm &operator*=(const Value &value);
	MathTerm &operator*=(const MathTerm &term);
	MathTerm &operator*=(Expresion &expresion);

	std::vector<Component> getComponents() const;
	size_t size();

	bool isKnown() const { return m_isKnown; }
	bool isCompact() const { return m_isCompact; }
	bool isEmpty() const { return m_isEmpty;  }

	MathTerm &add(const Value &value);
	MathTerm &multiplyKnown(const Value &known);
	bool addToKnown(size_t value);
	bool isMember(Component &component);
	ptrdiff_t index(const Component &component) const;
	ptrdiff_t index(const Value &value) const;

	MathTerm getCompact() const;
	bool compactate();
	void clear();

	inline bool checkHardEqual(const MathTerm &term) const {
		return t_sign == term.t_sign && getCompact().checkEasyEqual(term.getCompact());
	}

	bool unsortedEqual(const MathTerm &term) const;
	bool checkEasyEqual(const MathTerm &term) const;

	inline bool operator==(const MathTerm &term) const {
		return checkEasyEqual(term) || checkHardEqual(term);
	}

	std::vector<char> toString(bool child) const;
	std::string toHTMLString(bool child, std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const;
};

#endif
