#ifndef _MathExpresionBuilder
#define _MathExpresionBuilder
#include <memory>

class Expresion;
class MathTerm;
class Component;
struct Value;

namespace Utils {
	
	namespace Values {
		std::shared_ptr<Value> directSum(const Value &val1, const Value &val2);
	}

	namespace Terms {
		template<typename ...Args>
		MathTerm multiply(const Value &val1, const Value &val2, const Args... more);
	}
	
	namespace Expresions {
		Expresion getExpresion(Component &component);
		Expresion getExpresion(const MathTerm &term);

		Expresion multiply(const MathTerm &base, const std::vector<MathTerm> &multipliers);

		template<typename ...Args>
		Expresion multiply(const Value &val1, const Value &val2, const Args... more);

		template<typename ...Args>
		Expresion &sumUp(const Value &val1, const Value &val2, const Args... more);
	}
}

#endif
