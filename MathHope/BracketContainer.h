#ifndef _BracketContainer
#define _BracketContainer
#include <iostream>
#include "Expresion.h"
#include "InterpreterErrors.h"

namespace m_Interpreter {

	class ExpresionContainer;

	enum Bracket { ROUND, SQUARE, BRACES, NONE };

	struct Separator {
		bool s_isOpen = true;
		size_t s_begin, s_end;
		Bracket s_bracket;

		friend class ::Interpreter;

		void close();

	public:
		Separator();

		void close(size_t end);
		void init(size_t at, Bracket type);
		bool operator==(const Separator &other) const;
	};

	class BracketContainer {
		size_t b_level = 0;
		Separator b_content;
		size_t *b_current = nullptr;
		static vector<BracketContainer*> b_ptrs;
		vector<vector<size_t>> b_stacktrace;
		vector<BracketContainer*> b_childs;
		BracketContainer* b_parent = nullptr;

		friend class ::Interpreter;
		friend class ExpresionContainer;

		BracketContainer(BracketContainer *parent);

		void log(std::vector<size_t> &vec);
		void finalize();

	public:
		BracketContainer() {}

		BracketContainer(size_t *current, string raw);

		void dig(size_t at, const Bracket &type);
		void end(size_t at, const Bracket &type);
		bool checkDataIntegrity();

		string removeErrors(const string &original);
		string fixErrorsLast(const string &original);
		string fixErrorsFirst(const string &original);

		bool operator==(const BracketContainer &other) const;

		~BracketContainer();
	};
}
#endif