#include "Expresion.h"

Component::Component(const Value &val, const Value &exponent) : m_val(std::make_shared<Value>(val)), m_exponent(std::make_shared<Value>(exponent)) {}
Component::Component(std::shared_ptr<Value> &val, std::shared_ptr<Value> &exponent) : m_val(std::move(val)), m_exponent(std::move(exponent)) {}
Component::Component(const Value &val) : m_val(std::make_shared<Value>(val)), m_exponent(std::make_shared<Value>(1)) {}
Component::Component(std::shared_ptr<Value> &val) : m_val(std::move(val)), m_exponent(std::make_shared<Value>(1)) {}
Component::Component() : m_val(std::make_shared<Value>(1)), m_exponent(std::make_shared<Value>(1)) {}

Value &Component::getValue() const {
	return *m_val;
}

Value &Component::getExponent() const {
	return *m_exponent;
}

Value Component::getCompact() const {
	return *(*m_val ^ *m_exponent);
}

bool Component::hasExponent() const {
	return *m_exponent != Value::ONE();
}

bool Component::isCompactable() const {
	return m_val->isKnown() && m_exponent->isKnown();
}

bool Component::operator==(const Component &component) const {
	return *m_val == *component.m_val && *m_exponent == *component.m_exponent;
}

bool Component::operator+=(size_t value) {
	if (m_val->isKnown() && m_exponent->isKnown()) {
		if (*m_exponent != Value::ONE()) {
			m_val = *m_val ^ *m_exponent;
			m_exponent = std::make_shared<Value>(1);
		}
		m_val = *m_val + value;
	} else
		return false;
	return true;
}

bool Component::operator+=(const Value &value) {
	if (m_val->isKnown() && m_exponent->isKnown() && value.isKnown()) {
		if (*m_exponent != Value::ONE()) {
			m_val = *m_val ^ *m_exponent;
			m_exponent = std::make_shared<Value>(1);
		}
		m_val = *m_val + value;
	} else
		return false;
	return true;
}

void Component::sumToExponent(size_t exponent) {
	m_exponent = *m_exponent + exponent;
}

void Component::sumToExponent(const Value &value) {
	m_exponent = *m_exponent + value;
}

void Component::operator^=(size_t exponent) {
	m_exponent = *m_exponent * exponent;
}

Component &Component::operator*=(size_t value) {
	if (*m_exponent == Value::ONE())
		m_val = *m_val * value;
	else {
		m_val = std::make_shared<Value>(Expresion(MathTerm(*this) += Component(value)));
		m_exponent = std::make_shared<Value>(1);
	}

	return *this;
}

Component &Component::operator*=(const Component &comp) {
	if (operator==(comp))
		operator^=(2);
	else if (*m_val == *comp.m_val)
		m_exponent = *m_exponent + *comp.m_exponent;
	else {
		Component component = Component(comp);
		if (isCompactable() && component.isCompactable()) {
			m_val = getCompact() * component.getCompact();
			m_exponent = std::make_shared<Value>(1);
		} else if (isCompactable() && !component.hasExponent() && component.getValue().isComplex()) {
			Value &compact = getCompact();
			for (MathTerm term : (*component.getValue().complexValue).m_terms)
				term *= compact;
		} else if (component.isCompactable() && !hasExponent() && getValue().isComplex()) {
			Value &compact = component.getCompact();
			for (MathTerm term : (*getValue().complexValue).m_terms)
				term *= compact;
		} else {
			m_val = std::make_shared<Value>(Expresion(MathTerm(Component(*this)) += Component(component)));
			m_exponent = std::make_shared<Value>(1);
		}
	}

	return *this;
}

std::vector<char> Component::toString() const {
	std::vector<char> result;

	std::vector<char> base = m_val->toString();
	bool _brackets = brackets(base) && hasExponent();
	if (_brackets)
		result.push_back('(');

	result.insert(result.end(), base.begin(), base.end());

	if (_brackets)
		result.push_back(')');

	if (hasExponent()) {
		result.push_back('^');
		std::vector<char> exponent = m_exponent->toString();
		_brackets = brackets(exponent);
		if (_brackets)
			result.push_back('(');

		result.insert(result.end(), exponent.begin(), exponent.end());

		if (_brackets)
			result.push_back(')');
	}
	return result;
}

std::string Component::toHTMLString(std::size_t overall, std::vector<Utils::Entry<std::size_t, std::size_t>> &powIndexes) const {
	std::string result;

	std::string base = m_val->toHTMLString(overall, powIndexes);
	bool _brackets = brackets(base) && hasExponent();
	if (_brackets)
		result.push_back('(');

	result.insert(result.end(), base.begin(), base.end());

	if (_brackets)
		result.push_back(')');

	if (hasExponent()) {
		powIndexes.emplace_back(overall + result.size(), 5);
		result.append("<sup>");
		std::string exponent = m_exponent->toHTMLString(result.size() + overall, powIndexes);

		result.insert(result.end(), exponent.begin(), exponent.end());
		powIndexes.emplace_back(overall + result.size(), 6);
		result.append("</sup>");
	}
	return result;
}
