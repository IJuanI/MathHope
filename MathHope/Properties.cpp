#include "Properties.h"

std::vector<Utils::Entry<std::string, Property>> Properties::properties = std::vector<Utils::Entry<std::string, Property>>();


Property Properties::getProperty(std::string key) {
	auto iter = Utils::findConst(properties, (Func<Utils::Entry<std::string, Property>>)
		[&key](Utils::Entry<std::string, Property> entry) { return *entry.en_key == key; });
	if (iter == properties.cend())
		return Property::NONE();
	else
		return *iter->en_value;
}

void Properties::setProperty(std::string key, Property property) {
	auto iter = Utils::find(properties, (Func<Utils::Entry<std::string, Property>>)
		[&key](Utils::Entry<std::string, Property> entry) { return *entry.en_key == key; });
	if (iter == properties.cend())
		properties.emplace_back(key, property);
	else
		iter->en_value = std::make_shared<Property>(property);
}

Property getProperty(std::string key) {
	return Properties::getProperty(key);
}

void setProperty(std::string key, Property property) {
	Properties::setProperty(key, property);
}