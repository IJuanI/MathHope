#ifndef _MathOperations
#define _MathOperations

#include "Expresion.h"

namespace Operations {

	MathTerm simplify(const Component &comp);

	Expresion getCommon(const Expresion &source);

	Expresion getCommonByGroup(const Expresion &source, size_t div);
}

#endif