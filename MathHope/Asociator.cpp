#include "Asociator.h"
#include "Interpreter.h"

MathTerm Asociator::asociate(const MathTerm &term) const {
	MathTerm result(term.t_sign);

	size_t independient = 0;
	bool hasIndependient = false;

	for (Component &comp : term.getComponents()) {
		if (comp.isCompactable()) {
			if (independient == 0)
				independient += *comp.getCompact().healtyValue;
			else
				independient *= *comp.getCompact().healtyValue;
			hasIndependient = true;
			continue;
		}

		if (comp.getValue().isComplex()) {
			while (comp.m_val->isComplex() && comp.m_val->complexValue->size() == 1
				&& comp.m_val->complexValue->operator[](0).m_isKnown)
				comp.m_val = comp.m_val->complexValue->operator[](0).getCompact()[0].m_val;
			if (comp.getValue().isComplex())
				comp.m_val = std::make_shared<Value>(Asociator(*comp.getValue().complexValue).getResult());
		}
		if (comp.getExponent().isComplex()) {
			while (comp.m_exponent->isComplex() && comp.m_exponent->complexValue->size() == 1
				&& comp.m_exponent->complexValue->operator[](0).m_isKnown)
				comp.m_exponent = comp.m_exponent->complexValue->operator[](0).getCompact()[0].m_val;
			if (comp.getExponent().isComplex())
				comp.m_exponent = std::make_shared<Value>(Asociator(*comp.getExponent().complexValue).getResult());
		}

		ptrdiff_t index = result.index(comp);

		if (index == -1) {
			auto iter = find(result.t_components, (Func<Component>) [&comp](Component other)
				{ return other.getValue() == comp.getValue(); });
			if (iter != result.t_components.end())
				iter->m_exponent = *iter->m_exponent + *comp.m_exponent;
			else
				result << comp;
		} else
			result.t_components[index] ^= 2;
	}

	if (hasIndependient)
		result << Component(independient);

	return result;
}

Asociator::Asociator(const Expresion &expresion) {
	size_t independient = 0;
	bool hasIndependient = false;
	std::vector<MathTerm> firstStep;

	for (MathTerm term : expresion.getTerms()) {
		MathTerm &parsed = asociate(term);
		
		if (parsed.m_isKnown) {
			independient += *parsed.t_components[0].getValue().healtyValue;
			hasIndependient = true;
		} else {
			auto iter = find(firstStep, (Func<MathTerm>) [&parsed](MathTerm index) { return index.unsortedEqual(parsed); });
			if (iter == firstStep.end())
				firstStep.emplace_back(parsed);
			else
				firstStep[std::distance(firstStep.begin(), iter)] = parsed *= 2;
		}
	}
	
	std::vector<Entry<std::ptrdiff_t, std::vector<Component>>> storage;
	for (MathTerm &term : firstStep) {
		auto iter = find(term.t_components, (Func<Component>) [](Component comp) { return comp.isCompactable(); });

		if (iter != term.t_components.end()) {
			size_t index = std::distance(term.t_components.begin(), iter);
			std::ptrdiff_t val = *term.t_components[index].getCompact().healtyValue;
			std::vector<Component> others;
			for (size_t i = 0; i < term.t_components.size(); i++)
				if (i != index)
					others.push_back(term.t_components[i]);
			
			storage.emplace_back((term.t_sign ? val : -val), others);
		} else
			storage.emplace_back((term.t_sign ? 1 : -1), term.t_components);
	}
	
	std::vector<size_t> ignoreList;
	
	for (size_t i = 0; i < storage.size(); i++) {
		if (find(ignoreList, i) != ignoreList.end())
			continue;

		auto &entry = storage[i];
		auto &entryComponents = *entry.en_value;
		ptrdiff_t coefficient = *entry.en_key;

		for (size_t j = 0; j < storage.size(); j++)
			if (j != i && find(ignoreList, j) == ignoreList.end()) {
				auto &other = storage[j];
				auto otherComponents = *other.en_value;
				auto queue = entryComponents;
				bool clean = true;
				for (auto component : otherComponents) {
					auto searchIter = find(queue, component);
					if (searchIter == queue.end()) {
						clean = false;
						break;
					} else
						queue.erase(searchIter);
				}

				if (clean && queue.size() < 1) {
					coefficient += *other.en_key;
					ignoreList.push_back(j);
				}
			}

		if (coefficient != 0) {
			MathTerm term = MathTerm(coefficient >= 0);
			if (coefficient < 0)
				coefficient = -coefficient;

			if (coefficient != 1)
				term << Component(coefficient);

			for (Component comp : *entry.en_value)
				term << comp;

			as_result += term;
		}
	}

	if (hasIndependient)
		as_result += MathTerm(Component(independient));
}

Expresion &Asociator::getResult() {
	return as_result;
}
