#include "BracketContainer.h"

namespace m_Interpreter {
	void Separator::close() {
		s_isOpen = false;
	}

	Separator::Separator() : s_begin(0), s_end(0), s_bracket(NONE) { }

	void Separator::close(size_t end) {
		this->s_end = end;
		s_isOpen = false;
	}
	void Separator::init(size_t at, Bracket type) {
		s_begin = at;
		s_bracket = type;
	}

	bool Separator::operator==(const Separator &other) const {
		return s_isOpen == other.s_isOpen && s_begin == other.s_begin && s_end == other.s_end && s_bracket == other.s_bracket;
	}

	const char* initBracket(Bracket bracket) {
		switch (bracket) {
		case ROUND:
			return ")";
		case SQUARE:
			return "]";
		case BRACES:
			return "}";
		default:
			return '\0';
		}
	}
	const char* endBracket(Bracket bracket) {
		switch (bracket) {
		case ROUND:
			return ")";
		case SQUARE:
			return "]";
		case BRACES:
			return "}";
		default:
			return '\0';
		}
	}

	BracketContainer::BracketContainer(BracketContainer *parent) : b_current(parent->b_current), b_level(parent->b_level + 1), b_parent(parent) {
		parent->b_childs.emplace_back(this);
	}

	void BracketContainer::log(vector<size_t> &vec) {
		if (b_level != 0) {
			try {
				vec.insert(vec.begin(), distance(find(b_parent->b_childs, (Func<BracketContainer*>)
					[this](BracketContainer* item) { return item == this; }), b_parent->b_childs.begin()));
				b_parent->log(vec);
			} catch (runtime_error err) {
				throw runtime_error("Found an ilegal separator stored in a container without parent?!?");
			}
		}
	}

	void BracketContainer::finalize() {
		for (BracketContainer* child : b_childs)
			delete child;
	}

	vector<BracketContainer*> BracketContainer::b_ptrs = { };

	BracketContainer::BracketContainer(size_t *current, string raw) : b_level(0), b_current(current), b_parent(NULL) {
		b_content.s_begin = 0;
		b_content.s_end = raw.size();
	}

	void BracketContainer::dig(size_t at, const Bracket &type) {
		if (b_level == 0)
			(*b_current)++;
		else if (b_level == *b_current) {
			b_content.init(at, type);
			return;
		}

		for (BracketContainer* child : b_childs)
			if (child->b_content.s_isOpen) {
				child->dig(at, type);
				return;
			}

		(new BracketContainer(this))->dig(at, type);
	}

	void BracketContainer::end(size_t at, const Bracket &type) {
		if (b_level == *b_current)
			if (b_level == 0)
				throw interpreter_error(AllClosed, this); // #[{}])
			else
				if (b_content.s_isOpen)
					if (b_content.s_bracket == type)
						b_content.close(at);
					else
						throw interpreter_error(UnespectedBracket, this); // ([{#])
				else
					throw interpreter_error(AlreadyClosed, this); // ???, another error isn�t properly handled
		else {
			bool ended = false;

			for (BracketContainer* child : b_childs)
				if (child->b_content.s_isOpen) {
					child->end(at, type);
					ended = true;
					break;
				}

			if (!ended)
				throw interpreter_error(InternalError, this); // !?!?, Maybe you deleted the wrong pointer?

			if (b_level == 0)
				(*b_current)--;
		}
	}

	bool BracketContainer::checkDataIntegrity() {
		bool integrity = b_content.s_isOpen;

		if (!integrity)
			if (b_level == 0)
				throw runtime_error("Root class's content isn't open, but it doesn't even has content?");
			else if (b_parent == NULL) {
				throw runtime_error("Container doesn't seems to have parent, plus it's content is closed...");
			} else {
				vector<size_t> address;
				b_stacktrace.emplace_back(address);
				log(address);
			}

			for (BracketContainer* child : b_childs)
				if (integrity)
					integrity |= child->checkDataIntegrity();
				else
					child->checkDataIntegrity();

			return integrity;
	}

	string BracketContainer::removeErrors(const string &original) {
		string result = string(original);
		vector<size_t> changes;

		for (vector<size_t> error : b_stacktrace) {
			BracketContainer* actual = this;

			for (size_t dir : error)
				actual = actual->b_childs[dir];

			if (actual->b_content.s_bracket == NONE)
				throw runtime_error("Got an error at root?");
			else {
				size_t originalPos = actual->b_content.s_begin;
				size_t relativePos = originalPos;

				for (size_t changed : changes)
					if (changed < originalPos)
						relativePos--;

				changes.emplace_back(originalPos);
				result.erase(relativePos, relativePos + 1);
			}
		}

		return result;
	}

	string BracketContainer::fixErrorsLast(const string &original) {
		string result = string(original);
		vector<size_t> changes;

		for (vector<size_t> error : b_stacktrace) {
			BracketContainer* actual = this;

			for (size_t dir : error)
				actual = actual->b_childs[dir];

			if (actual->b_content.s_bracket == NONE)
				throw runtime_error("Got an error at root?");
			else {
				auto parent = actual->b_parent;
				if (parent->b_content.s_bracket == NONE) {
					result.append(endBracket(actual->b_content.s_bracket));
					changes.emplace_back(result.size() - 1);
				} else {
					size_t originalPos = parent->b_content.s_end;
					size_t relativePos = originalPos;

					for (size_t changed : changes)
						if (changed < originalPos)
							relativePos++;
					changes.emplace_back(originalPos - 1);
					result.insert(relativePos - 1, endBracket(actual->b_content.s_bracket));
				}
			}
		}

		return result;
	}

	string BracketContainer::fixErrorsFirst(const string &original) {
		string result = string(original);
		vector<size_t> changes;

		for (vector<size_t> error : b_stacktrace) {
			BracketContainer* actual = this;

			for (size_t dir : error)
				actual = actual->b_childs[dir];

			if (actual->b_content.s_bracket == NONE)
				throw runtime_error("Got an error at root?");
			else {
				size_t originalPos;
				auto parent = actual->b_parent;
				auto childs = parent->b_childs;
				if (error.at(error.size() - 1) == childs.size() - 1)
					if (parent->b_content.s_bracket == NONE) {
						result.append(endBracket(actual->b_content.s_bracket));
						changes.emplace_back(result.size() - 1);
						continue;
					} else
						originalPos = parent->b_content.s_end;
				else
					originalPos = childs.at(error.at(error.size() - 1))->b_content.s_end;

				size_t relativePos = originalPos;

				for (size_t changed : changes)
					if (changed < originalPos)
						relativePos++;

				changes.emplace_back(originalPos - 1);
				result.insert(relativePos - 1, endBracket(actual->b_content.s_bracket));
			}
		}

		return result;
	}

	bool BracketContainer::operator==(const BracketContainer &other) const {
		return b_content == other.b_content && b_level == other.b_level;
	}

	BracketContainer::~BracketContainer() {
		finalize();
	}
}
